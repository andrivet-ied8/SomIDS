/*
 * @file         progress.c
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-04-09
 */

#include "somit.h"

static int last_progress = -1;

/**
 * Affiche une barre de progression.
 *
 * @param step Le pas à afficher.
 * @param steps Le nombre total de pas.
 */
void progress_step(int step, int steps) {
  if(last_progress == -1) {
    printf("| Progression .....................................|\n");
    printf("|");
    last_progress = 0;
  }

  int progress = 100 * (step + 1) / steps;
  if(progress < last_progress) return;

  int chars = (progress - last_progress) / 2;
  if(chars <= 0) return;

  for(int i = 0; i < chars; ++i)
    printf("#");
  fflush(stdout);

  if(progress == 100) printf("| Terminé\n");

  last_progress = progress;
}
