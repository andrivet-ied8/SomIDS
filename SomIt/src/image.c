/*
 * @file         image.c
 * @brief        L3 IA et Apprentissage - Options de la ligne de commande
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-01-08
 */

#include <png.h>
#include "somit.h"

/**
 * Construit le chemin d'une image.
 *
 * @param filename Le nom du fichier.
 * @param out_dir Le répertoire où si situe le fichier.
 * @return Le chemin complet.
 */
const char *construct_image_path(const char *filename, const char *out_dir) {
  size_t filename_length = strlen(filename);
  size_t out_dir_length = strlen(out_dir);
  char *path = malloc(out_dir_length + filename_length + 2);
  strcpy(path, out_dir);
  if(path[out_dir_length] != '/') strcat(path, "/");
  strcat(path, filename);
  return path;
}

/**
 * Met un pixel dans un tampon pour une image PNG.
 *
 * @param pixel Pointeur sur le pixel.
 * @param r La composante rouge.
 * @param g La composante verte.
 * @param b La composante bleue,
 * @return Pointeur sur le pixel suivant,
 */
inline static png_bytep set_pixel(png_bytep pixel, png_byte r, png_byte g, png_byte b) {
  *(pixel++) = r;   // Rouge
  *(pixel++) = g;   // Vert
  *(pixel++) = b;   // Bleu
  *(pixel++) = 255; // Transparence
  return pixel;
}

/**
 * Met un rectangle 1xn de pixels dans un tampon pour une image PNG.
 *
 * @param pixel Pointeur sur le premier pixel.
 * @param r La composante rouge.
 * @param g La composante verte.
 * @param b La composante bleue,
 * @param zoom La largeur du rectangle.
 * @return Pointeur sur le pixel suivant,
 */
png_bytep set_pixels(png_bytep pixel, double weight, int zoom, PALETTE palette) {
  png_byte r, g, b;
  colormap_convert(weight, palette, &r, &g, &b);

  for(int i = 0; i < zoom; ++i)
    pixel = set_pixel(pixel, r, g, b);
  return pixel;
}

/**
 * Affiche une marque dans une image.
 *
 * @param image L'image.
 * @param y
 * @param x
 * @param r
 * @param g
 * @param b
 * @param zoom
 * @param nb_columns
 */
void show_mark(png_bytep image, int y, int x, unsigned char r,  unsigned char g,  unsigned char b, int zoom, int nb_columns) {
  int mark_size = zoom > 2 ? zoom / 2 : 1;
  int margin = mark_size / 2;
  int row_size = nb_columns * zoom;

  int image_y = 4 * y * zoom * row_size;
  int margin_y = 4 * margin * row_size;
  int image_x = 4 * x * zoom;
  int margin_x = 4 * margin;

  png_bytep pixel = image + image_y + margin_y + image_x + margin_x;
  for(int j = 0; j < mark_size; ++j) {
    for(int i = 0; i < mark_size; ++i) {
      *(pixel++) = r;
      *(pixel++) = g;
      *(pixel++) = b;
      *(pixel++) = 0xFF;
    }
    pixel += 4 * row_size - mark_size * 4;
  }
}

/**
 *
 * @param image
 * @param umatrix
 * @param options
 */
void show_winners(png_bytep image, const UMatrix *umatrix, const SaveOptions *options) {
  for(int i = 0; i < options->nb_winners; ++i) {
    int winner = options->winners[i];
    int l = winner / umatrix->map_nb_columns;
    int c = winner % umatrix->map_nb_columns;
    int umatrix_l = l * umatrix->nb_lines / umatrix->map_nb_lines;
    int umatrix_c = c * umatrix->nb_columns / umatrix->map_nb_columns;
    const Class *cls = options->classes + i;
    show_mark(image, umatrix_l, umatrix_c, cls->r, cls->g, cls->b, options->zoom, umatrix->nb_columns);
  }
}

/**
 *
 * @param umatrix
 * @param options
 * @return
 */
bool umatrix_save_image(const UMatrix *umatrix, const SaveOptions *options) {
  const char *path = construct_image_path(options->filename, options->out_dir);

  FILE *fp = fopen(path, "wb");
  if(!fp) {
    print_error("Erreur lors de l'ouverture du fichier");
    free((void*)path);
    return false;
  }

  png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!png) {
    print_error("Erreur lors de la creation de l'image");
    fclose(fp);
    free((void*)path);
    return false;
  }

  png_infop info = png_create_info_struct(png);
  if (!info) {
    print_error("Erreur lors de la creation de l'image");
    fclose(fp);
    free((void*)path);
    return false;
  }

  png_init_io(png, fp);

  png_set_IHDR(
      png,
      info,
      umatrix->nb_columns * options->zoom,
      umatrix->nb_lines * options->zoom,
      8,
      PNG_COLOR_TYPE_RGBA,
      PNG_INTERLACE_NONE,
      PNG_COMPRESSION_TYPE_DEFAULT,
      PNG_FILTER_TYPE_DEFAULT
  );
  png_write_info(png, info);

  // 4 octets par pixel - RGBA
  size_t row_size = 4 * umatrix->nb_columns * options->zoom * sizeof(png_byte);
  png_bytep image = (png_bytep)checked_malloc(umatrix->nb_lines * options->zoom * row_size);
  png_byte **rows = (png_byte**)checked_malloc(umatrix->nb_lines * options->zoom * sizeof(png_byte*));
  if(!image || !rows) {
    fclose(fp);
    free((void*)path);
    return false;
  }

  const double *weights = umatrix->weights;
  png_bytep pixel = image;

  for(int l = 0; l < umatrix->nb_lines; l++) {
    int row = l * options->zoom;
    rows[row] = pixel;
    for(int c = 0; c < umatrix->nb_columns; c++, ++weights) {
      pixel = set_pixels(pixel, *weights, options->zoom, options->palette);
    }
    for(int i = 1; i < options->zoom; ++i) {
      rows[row + i] = pixel;
      memmove(pixel, rows[row], row_size);
      pixel += row_size;
    }
  }

  if(options->winners != NULL)
    show_winners(image, umatrix, options);

  png_write_image(png, rows);

  png_free_data(png, info, PNG_FREE_ALL, -1);
  png_destroy_write_struct(&png, (png_infopp)NULL);
  free(rows);
  free(image);

  fclose(fp);
  free((void*)path);
  return true;
}

