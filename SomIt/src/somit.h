/*
 * @file         SomIDS.h
 * @brief        L3 IA et Apprentissage - Options de la ligne de commande
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-01-08
 */

#ifndef SOMIDS_H
#define SOMIDS_H

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include "somlib.h"

/** Options de l'outil en ligne de commande **/
typedef struct {
  const char *input_filename; //< Nom du fichier d'entrées.
  const char *out_folder; //< Nom du répertoire oú créer les images (sorties).
  int nb_iterations; //< Nombre d'itérations.
  int radius; //< Rayon du voisinage initial.
  double alpha; //< Coefficient (taux) d'apprentissage initial.
  int map_width; //< Largeur de la carte
  int map_height; //< Longueur de la carte
  double init_min; //< Valeur minimale en dessous de la moyenne pour l'initialisation
  double init_max; //< Valeur maximale en dessus de la moyenne pour l'initialisation
  int zoom; //< Facteur de zoom pour les images générées
  bool progress; //< Affiche la progression
} Options;

/** Options de sauvegarde **/
typedef struct {
  char filename[NAME_MAX]; //< Nom du fichier
  char out_dir[PATH_MAX]; //< Répertoire oú sauver le fichier
  int zoom; //< Nombre de pixels par cellule
  PALETTE palette; //< Palette de couleurs à utiliser
  const int *winners; //< Les indices des neurones gagnants (BMU) lors de l'apprentissage depuis les entrées
  int nb_winners; //< Nombre d'éléments dans winners
  Class *classes;  //< Les classes du fichier d'entrées (une classe par échantillon).
  int nb_samples; //< Le nombre de classes dans classes.
  int nb_classes; //< Le nombre de classes différentes
} SaveOptions;

/** Options **/
void options_show(void);
Options *options_parse(int argc, char **argv);
void options_destroy(Options *options);

/** Carte (matrice) de neurones **/
void map_display_weights(const Map *map);

/** U-Matrix **/
bool umatrix_save_image(const UMatrix *umatrix, const SaveOptions *options);

void progress_step(int step, int steps);


#endif // SOMIDS_H
