/*
 * @file         options.c
 * @brief        L3 IA et Apprentissage - Options de la ligne de commande
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-01-08
 */

#include <unistd.h>
#include <sys/stat.h>
#include "somit.h"

/**
 * Affichage d'une aide avec les différentes options possibles.
*/
void options_show(void) {
  printf("\nSom - Une implementation de l'algorithme SOM.\n");
  printf("\nUtilisation: Som [options] fichier \n");
  printf("\nOptions:\n");
  printf("  -o / --output: Repertoire ou ecrire les images (par default: pas de génération d'images)\n");
  printf("  -p / --progress: Affiche la progression des iterations (par default: non)\n");
  printf("  -i / --iterations: Nombre d'iterations (par default: 500)\n");
  printf("  -r / --radius: Rayon de depart (par default: 20)\n");
  printf("  -a / --alpha: Facteur d'apprentissage (par default: 0.9)\n");
  printf("  -w / --width: Taille de la carte en largeur (par default: 20)\n");
  printf("  -h / --height: Taille de la crate en hauteur (par default: 20)\n");
  printf("  -n / --min: Valeur minimum en dessous de la moyenne lors de l'initialisation de la carte (par default: 0.1)\n");
  printf("  -x / --max: Valeur maximum en dessus de la moyenne lors de l'initialisation de la carte (par default: 0.9)\n");
  printf("  -z / --zoom: Taille en pixel d'une cellule de la carte (par default: 10)\n");
  printf("\nArgument:\n");
  printf("  fichier: Chemin du fichier contenant les entrees.\n\n");
}

int strtoi(const char *str, char **str_end, int base) {
  long value = strtol(str, str_end, base);
  if(value >= INT_MIN && value <= INT_MAX) return (int)value;
  *str_end = (char*)str;
  return 0;
}


Options *options_parse(int argc, char **argv) {
  static struct option long_options[] = {
    {"output",      required_argument, 0, 'o'},
    {"progress",    no_argument,       0, 'p'},
    {"iterations",  required_argument, 0, 'i'},
    {"radius",      required_argument, 0, 'r'},
    {"alpha",       required_argument, 0, 'a'},
    {"width",       required_argument, 0, 'w'},
    {"height",      required_argument, 0, 'h'},
    {"min",         required_argument, 0, 'n'},
    {"max",         required_argument, 0, 'x'},
    {"zoom",        required_argument, 0, 'z'},
    {0,             0,                 0,  0}
  };

  Options *options = checked_malloc(sizeof(Options));
  if(!options) return NULL;

  options->out_folder = NULL;
  options->nb_iterations = 500;
  options->radius = 20;
  options->alpha = 0.9;
  options->map_width = 20;
  options->map_height = 20;
  options->init_min = 0.1;
  options->init_max = 0.9;
  options->zoom = 10;
  options->progress = false;

  int option_index = 0;
  char *end = NULL;

  for(;;) {
    int c = getopt_long(argc, argv, "o:i:r:a:w:h:n:x:z:p", long_options, &option_index);
    if(c == -1) break;
    switch(c) {
      case 'o':
        options->out_folder = optarg;
        break;

      case 'i':
        options->nb_iterations = strtoi(optarg, &end, 10);
        if(*end != 0 || options->nb_iterations <= 0) {
          print_error("Nombre d'iterations invalide: %s", optarg);
          free(options);
          return NULL;
        }
        break;

      case 'r':
        options->radius = strtoi(optarg, &end, 10);
        if(*end != 0 || options->radius <= 0) {
          print_error("Rayon invalide: %s", optarg);
          free(options);
          return NULL;
        }
        break;

      case 'a':
        options->alpha = strtod(optarg, &end);
        if(*end != 0 || options->alpha <= 0 || options->alpha > 1) {
          print_error("Alpha invalide: %s", optarg);
          free(options);
          return NULL;
        }
        break;

      case 'w':
        options->map_width = strtoi(optarg, &end, 10);
        if(*end != 0 || options->map_width <= 0) {
          print_error("Largeur invalide: %s", optarg);
          free(options);
          return NULL;
        }
        break;

      case 'h':
        options->map_height = strtoi(optarg, &end, 10);
        if(*end != 0 || options->map_height <= 0) {
          print_error("Hauteur invalide: %s", optarg);
          free(options);
          return NULL;
        }
        break;

      case 'n':
        options->init_min = strtod(optarg, &end);
        if(*end != 0 || options->init_min < 0) {
          print_error("Minimum invalide: %s", optarg);
          free(options);
          return NULL;
        }
        break;

      case 'x':
        options->init_max = strtod(optarg, &end);
        if(*end != 0 || options->init_max < 0) {
          print_error("Maximum invalide: %s", optarg);
          free(options);
          return NULL;
        }
        break;

      case 'z':
        options->zoom = strtoi(optarg, &end, 10);
        if(*end != 0 || options->zoom <= 0) {
          print_error("Zoom invalide: %s", optarg);
          free(options);
          return NULL;
        }
        break;

      case 'p':
        options->progress = true;
        break;

      default:
        print_error("Option inconnue: %c", c);
        free(options);
        return NULL;
    }
  }

  if(optind >= argc) {
    print_error("Il manque en argument le nom ou le chemin du fichier des entrées");
    free(options);
    return NULL;
  }
  if(argc - optind != 1) {
    print_error("Il y a trop d'arguments après les options");
    free(options);
    return NULL;
  }

  const char *filename = argv[optind];
  if(access(filename, R_OK) != 0) {
    free(options);
    print_last_error("Le fichier '%s' n'est pas lisible", filename);
    return NULL;
  }
  options->input_filename = filename;

  if(access(options->out_folder, R_OK) != 0) {
    print_last_error("Le répertoire '%s' n'est pas accessible", options->out_folder);
    free(options);
    return NULL;
  }
  struct stat s;
  if(stat(options->out_folder, &s) != 0 || !S_ISDIR(s.st_mode)) {
    print_last_error("'%s' n'est pas un répertoire", options->out_folder);
    free(options);
    return NULL;
  }

  return options;
}

void options_destroy(Options *options) {
  free(options);
}
