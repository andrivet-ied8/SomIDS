/*
 * @file         main.c
 * @brief        L3 IA et Apprentissage - Implémentation de l'algorithme SOM
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-01-08
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "somit.h"


/**
 * Point d'entrée du programme.
*/
int main(int argc, char **argv) {
  Options *options = options_parse(argc, argv);
  if (NULL == options) {
    options_show();
    return 0;
  }

  // Lecture des entrées depuis un fichier
  if(options->progress) printf("Lecture du fichier d'entrées\n");
  Input *input = input_read(options->input_filename);
  if(!input) return 1;

  // Normalisation des entrées
  if(options->progress) printf("Normalisation des entrées\n");
  input_normalize(input);

  // Calcule d'un vecteur moyen
  if(options->progress) printf("Calcul du vecteur moyen\n");
  double *means = vector_compute_means(input->values, input->nb_dimensions, input->nb_samples);
  if(!means) {
    input_destroy(input);
    return 1;
  }

  // Initialisation de la carte avec des valeurs aléatoires autour du vecteur moyen
  if(options->progress) printf("Génération de la carte\n");
  Map *map = map_generate(
      options->map_height,options->map_width,
      input->nb_dimensions,
      INIT_MODE_AVERAGE, means, options->init_min, options->init_max);
  if(!map) {
    free(means);
    input_destroy(input);
    return 1;
  }

  Winners *winners = winners_init(input->nb_samples);
  if(!winners) {
    map_destroy(map);
    free(means);
    input_destroy(input);
    return 1;
  }

  // Entrainement
  if(options->progress) printf("Entrainement\n");
  Training *training = training_init(
      input->nb_samples, map_nb_neurones(map), options->nb_iterations,
      options->radius, options->radius, options->alpha,
      DECAY_MODE_LINEAR, DECAY_MODE_LINEAR, DECAY_MODE_LINEAR,
      NEIGHBORHOOD_FUNCTION_BUBBLE
  );
  if(!training) {
    winners_destroy(winners);
    map_destroy(map);
    free(means);
    input_destroy(input);
    return 1;
  }

  while(training_next(map, training, winners, input)) {
    if(options->progress)
      progress_step(training->iteration, training->nb_iterations);

    if(options->out_folder) {
      SaveOptions save_options;
      sprintf(save_options.filename, "SOM-%04i.png", training->iteration);
      strcpy(save_options.out_dir, options->out_folder);
      save_options.zoom = options->zoom;
      save_options.palette = PALETTE_DARK_BODY | PALETTE_INVERT;
      save_options.winners = winners->neurons;
      save_options.nb_winners = winners->nb_winners;
      save_options.classes = input->classes;
      save_options.nb_samples = input->nb_samples;
      save_options.nb_classes = input->nb_classes;

      UMatrix *umatrix = umatrix_generate(map);
      umatrix_normalize(umatrix);
      umatrix_save_image(umatrix, &save_options);
      umatrix_destroy(umatrix);
    }
  }

  // On rend la mémoire
  winners_destroy(winners);
  map_destroy(map);
  free(means);
  input_destroy(input);
  options_destroy(options);
}
