cmake_minimum_required(VERSION 3.16)
project(SomIt VERSION 1.0 LANGUAGES C)

set(CMAKE_C_STANDARD 11)

find_package(PNG REQUIRED)

add_executable(SomIt
        src/main.c
        src/options.c
        src/somit.h
        src/image.c
        src/progress.c
)
target_compile_options(SomIt PUBLIC "$<$<CONFIG:DEBUG>:-Wall;-Wextra;-pedantic;-Wshadow>")

target_include_directories(SomIt PUBLIC ${PNG_INCLUDE_DIRS})
target_link_libraries(SomIt PRIVATE SomLib ${PNG_LIBRARIES})
