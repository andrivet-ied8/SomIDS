import QtCore 6.5
import QtQuick 6.5
import QtQuick.Controls.Fusion 6.5
import QtQuick.Layouts 6.5
import QtQuick.Dialogs 6.5
import com.andrivet.somplay 1.0

ApplicationWindow {
    title: "SomPlay"
    visible: true
    width: 1000
    height: 880
    minimumHeight: 200
    minimumWidth: 500
    color: "#333333"

    function setTrainingProperties() {
        som.mapWidth = properties.mapWidth
        som.mapHeight = properties.mapHeight
        som.iterations = properties.iterations
        som.radius0 = properties.radius0
        som.sigma0 = properties.sigma0
        som.alpha0 = properties.alpha0
        som.palette = properties.palette
        som.initMode = properties.initMode
        som.initMin = +properties.initMin
        som.initMax = +properties.initMax
        som.radiusDecay = properties.radiusDecay
        som.sigmaDecay = properties.radiusDecay // Same than radius
        som.alphaDecay = properties.alphaDecay
        som.neighborhoodFunction = properties.neighborhoodFunction
        statusBar.running = true;
    }

    function setDisplayProperties() {
        som.palette = properties.palette
        som.invertPalette = properties.invertPalette
        som.displayTrainingWinners = properties.displayTrainingWinners
        som.displayAnalyzingWinners = properties.displayAnalyzingWinners
        scale.palette = properties.palette
        scale.invertPalette = properties.invertPalette
    }

    ColumnLayout {
        spacing: 0
        anchors.fill: parent

        RowLayout {
            spacing: 0
            Layout.fillWidth: true
            Layout.fillHeight: true

            AppToolbar {
                id: toolbar
                onOpenFile: filepath => som.openFile(filepath)
                onSaveFile: filepath => som.saveFile(filepath)
                onTrainFile: filepath => {
                    setTrainingProperties();
                    som.trainFile(filepath);
                }
                onAnalyzeFile: (filepath) => {
                    statusBar.running = true;
                    som.analyzeFile(filepath);
                }
            }

            ColumnLayout {
                spacing: 0

                AppFeatures {
                    id: features
                    onClicked: (index) => som.displayFeature = index
                }

                RowLayout {
                    spacing: 0

                    SelfOrganizingMap {
                        id: som
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.minimumHeight: 150
                        Layout.minimumWidth: 150

                        onProgress: (message, step, steps) => {
                            statusBar.progressMessage = message
                            statusBar.progressTo = steps;
                            statusBar.progressValue = step;
                        }

                        onError: (error) => {
                            statusBar.running = false
                            statusBar.progressValue = 0
                            messageDialog.text = error;
                            messageDialog.open();
                        }

                        onFinished: {
                            statusBar.progressValue = 0
                            statusBar.running = false
                        }
                    }

                    AppScale {
                        id: scale
                    }
                }

                AppClasses {
                    id: classes
                }
            }

            Rectangle {
                Layout.fillHeight: true
                width: 1
                color: "dimgray"
            }

            AppProperties {
                id: properties
                onTrainPropertiesChanged: {
                    if(som.loaded)
                        statusBar.running = true
                }
                onApplyTrainProperties:  {
                    setTrainingProperties();
                    som.refreshTraining()
                }
                onDisplayPropertiesChanged: {
                    setDisplayProperties();
                    som.refreshDisplay();
                }
            }
        }

        AppStatusbar {
            id: statusBar
            running: false
        }
    }

    AppMessage {
        id: messageDialog
    }

    Binding {
        target: features
        property: "model"
        value: som.features
    }

    Binding {
        target: classes
        property: "model"
        value: som.classes
    }
}
