import QtCore 6.5
import QtQuick 6.5
import QtQuick.Controls.Fusion 6.5
import QtQuick.Layouts 6.5


Rectangle {
    id: properties

    signal applyTrainProperties()
    signal trainPropertiesChanged()
    signal displayPropertiesChanged()

    property alias mapWidth: sliderWidth.currentValue
    property alias mapHeight: sliderHeight.currentValue
    property alias iterations: sliderIterations.currentValue
    property alias radius0: sliderRadius.currentValue
    property alias sigma0: sliderSigma.currentValue
    property alias alpha0: sliderAlpha.currentValue
    property int initMode: 2
    property alias initMin: initMin.text
    property alias initMax: initMax.text
    property alias radiusDecay: radiusDecay.currentIndex
    property alias alphaDecay: alphaDecay.currentIndex
    property alias neighborhoodFunction: neighborhoodFunction.currentIndex

    property alias palette: palette.currentIndex
    property alias invertPalette: invertPalette.checked
    property alias displayTrainingWinners: displayTrainingWinners.checked
    property alias displayAnalyzingWinners: displayAnalyzingWinners.checked

    color: "#343434"
    border.width: 0
    Layout.minimumWidth: 250
    Layout.preferredWidth: 250
    Layout.fillHeight: true
    Layout.margins: 18

    Timer {
        id: trainProprertiesTimer
        interval: 2000; running: false; repeat: false
        onTriggered: properties.applyTrainProperties()
    }

    function trainPropertyChanged() {
        trainPropertiesChanged()
        trainProprertiesTimer.restart()
    }

    ScrollView {
        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff

        ColumnLayout {
            Layout.fillWidth: true

            PropertySlider {
                id: sliderWidth
                label: "Largeur"
                min: 10
                max: 200
                step: 1
                currentValue: 42
                onValueChanged: trainPropertyChanged()
            }

            PropertySlider {
                id: sliderHeight
                Layout.topMargin: 8
                label: "Hauteur"
                min: 10
                max: 200
                step: 1
                currentValue: 42
                onValueChanged: trainPropertyChanged()
            }

            PropertySlider {
                id: sliderIterations
                Layout.topMargin: 8
                label: "Itérations"
                min: 100
                max: 5000
                step: 10
                currentValue: 500
                onValueChanged: trainPropertyChanged()
            }

            PropertySlider {
                id: sliderRadius
                Layout.topMargin: 8
                label: "Rayon initial"
                valueFormat: "{}%"
                min: 1
                max: 100
                step: 1
                currentValue: 100
                onValueChanged: trainPropertyChanged()
            }

            PropertySlider {
                id: sliderSigma
                Layout.topMargin: 8
                label: "σ initial"
                valueFormat: "{}%"
                min: 1
                max: 100
                step: 1
                currentValue: 50
                onValueChanged: trainPropertyChanged()
            }

            Label {
                text: "Evolution du rayon/σ"
                Layout.topMargin: 8
            }

            ComboBox {
                id: radiusDecay
                Layout.preferredWidth: 180
                model: ["Linéaire", "Inversement décroissant", "Décroissance exponentielle"]
                currentIndex: 0
                onActivated: trainPropertyChanged()
            }

            PropertySlider {
                id: sliderAlpha
                Layout.topMargin: 8
                label: "Facteur d'apprentissage initial"
                min: 0.01
                max: 0.99
                step: 0.01
                currentValue: 0.1
                onValueChanged: trainPropertyChanged()
            }

            Label {
                text: "Evolution du facteur d'apprentissage"
                Layout.topMargin: 8
            }

            ComboBox {
                id: alphaDecay
                Layout.preferredWidth: 180
                model: ["Linéaire", "Inversement décroissant", "Décroissance exponentielle"]
                currentIndex: 0
                onActivated: trainPropertyChanged()
            }

            Label {
                text: "Fonction de voisinage"
                Layout.topMargin: 8
            }

            ComboBox {
                id: neighborhoodFunction
                Layout.preferredWidth: 180
                model: ["A bulle", "Gaussienne", "Chapeau mexicain", "Epanechnikov"]
                currentIndex: 0
                onActivated: trainPropertyChanged()
            }

            Label {
                text: "Poids initials des neurones"
                Layout.topMargin: 8
            }

            ButtonGroup { id: radioGroup }

            RadioButton {
                text: "Zéro"
                ButtonGroup.group: radioGroup
                onClicked:  {
                    properties.initMode = 0
                    trainPropertyChanged()
                }
            }

            RadioButton {
                text: "Aléatoire"
                ButtonGroup.group: radioGroup
                onClicked: {
                    properties.initMode = 1
                    trainPropertyChanged()
                }
            }

            RadioButton {
                id: averageButton
                text: "Moyenne"
                ButtonGroup.group: radioGroup
                checked: true
                onClicked:  {
                    properties.initMode = 2
                    trainPropertyChanged()
                }
            }

            RowLayout {
                Layout.fillWidth: true

                Label {
                    text: "Min:"
                }

                TextField {
                    id: initMin
                    text: "0.5"
                    Layout.preferredWidth: 50
                    enabled: averageButton.checked
                }

                Label {
                    text: "Max:"
                    Layout.leftMargin: 8
                }

                TextField {
                    id: initMax
                    text: "0.5"
                    Layout.preferredWidth: 50
                    enabled: averageButton.checked
                }
            }

            Label {
                text: "Gagnants (BMU)"
                Layout.topMargin: 8
            }

            CheckBox {
                id: displayTrainingWinners
                text: "Afficher pour l'apprentissage"
                checked: true
                onClicked: displayPropertiesChanged()
            }

            CheckBox {
                id: displayAnalyzingWinners
                text: "Afficher pour l'analyse"
                checked: true
                onClicked: displayPropertiesChanged()
            }

            Label {
                text: "Palette de couleurs"
                Layout.topMargin: 8
            }

            ComboBox {
                id: palette
                Layout.preferredWidth: 180
                model: ["Arc-en-ciel", "Corps noir", "Niveaux de gris", "Bleu Orange", "Bleu rouge", "Viridis", "Inferno"]
                currentIndex: 5
                onActivated: displayPropertiesChanged()
            }

            CheckBox {
                id: invertPalette
                text: "Inverser la palette de couleurs"
                checked: false
                onClicked: displayPropertiesChanged()
            }
        }
    }
}
