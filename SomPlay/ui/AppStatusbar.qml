import QtCore 6.5
import QtQuick 6.5
import QtQuick.Controls.Fusion 6.5
import QtQuick.Layouts 6.5

Rectangle {
    id: statusBar

    property alias progressMessage: message.text
    property alias progressTo: progress.to
    property alias progressValue: progress.value
    property alias running: busy.running

    color: "#602676"
    border.width: 0
    Layout.fillWidth: true
    Layout.minimumHeight: 28
    Layout.preferredHeight: 32

    RowLayout {
        anchors.fill: parent

        Label {
            id: message
            Layout.leftMargin: 8
            Layout.topMargin: 6
            Layout.minimumWidth: 50
            Layout.preferredWidth: 200
            Layout.fillHeight: true
            text: "Prêt"
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "transparent"
        }

        BusyIndicator {
            id: busy
            implicitHeight: 28
            implicitWidth: 28
        }

        ProgressBar {
            id: progress
            Layout.rightMargin: 8
            Layout.minimumWidth: 50
            Layout.preferredWidth: 200
            Layout.alignment: Qt.AlignRight + Qt.AlignVCenter
            implicitHeight: 20
            visible: busy.running
        }
    }
}
