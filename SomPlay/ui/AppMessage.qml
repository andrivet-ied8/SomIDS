import QtCore 6.5
import QtQuick 6.5
import QtQuick.Controls.Fusion 6.5
import QtQuick.Layouts 6.5
import QtQuick.Dialogs 6.5

MessageDialog {
    id: messageDialog
    title: "SomPlay"
    buttons: MessageDialog.Ok
}
