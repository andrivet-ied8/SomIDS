import QtCore 6.5
import QtQuick 6.5
import QtQuick.Controls.Fusion 6.5
import QtQuick.Layouts 6.5

Rectangle {
    property alias model: listClasses.model

    color: "black"
    Layout.fillWidth: true
    Layout.fillHeight: false
    Layout.preferredHeight: 28

    ListView {
        id: listClasses
        anchors.fill: parent
        orientation: ListView.Horizontal
        clip: true
        spacing: 8

        delegate: ItemDelegate {
            icon.source: "dot"
            icon.color: modelData.color
            text: modelData.name
            background: Rectangle {
                color: "black"
            }
        }
    }
}
