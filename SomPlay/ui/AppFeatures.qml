import QtCore 6.5
import QtQuick 6.5
import QtQuick.Controls.Fusion 6.5
import QtQuick.Layouts 6.5

TabBar {
    signal clicked(int index)
    property alias model: tabBarRepeater.model

    id: tabBar
    Layout.fillWidth: true
    Layout.preferredHeight: 40
    contentHeight: 40
    clip: true

    TabButton {
        text: "U-Matrice"
        icon.source: "dot"
        icon.color: checked ? "white" : "gray"
        width: 120
        onClicked: tabBar.clicked(-1);
    }

    Repeater {
        id: tabBarRepeater

        TabButton {
            text: modelData.name
            icon.source: "dot"
            icon.color: checked ? "white" : "gray"
            width:120
            onClicked: tabBar.clicked(modelData.index);
        }
    }
}
