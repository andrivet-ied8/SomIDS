import QtCore 6.5
import QtQuick 6.5
import QtQuick.Controls.Fusion 6.5
import QtQuick.Layouts 6.5
import com.andrivet.somplay 1.0

Rectangle {
    property alias palette: scale.palette
    property alias invertPalette: scale.invertPalette

    color: "black"
    Layout.minimumWidth: 8
    Layout.preferredWidth: 28
    Layout.fillHeight: true
    Layout.fillWidth: false

    ColumnLayout {
        spacing: 0
        anchors.fill: parent

        Text {
            text: "1.0"
            color: "white"
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
        }

        SomScale {
            id: scale
            Layout.margins: 4
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Text {
            text: "0.0"
            color: "white"
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
        }
    }
}
