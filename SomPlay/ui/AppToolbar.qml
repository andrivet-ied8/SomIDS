import QtCore 6.5
import QtQuick 6.5
import QtQuick.Controls.Fusion 6.5
import QtQuick.Layouts 6.5
import Qt.labs.platform

ToolBar {
    id: buttonsBar

    signal openFile(string filepath)
    signal saveFile(string filepath)
    signal trainFile(string filepath)
    signal analyzeFile(string filepath)

    Layout.minimumWidth: 68
    Layout.preferredWidth: 68
    Layout.fillHeight: true

    ColumnLayout {
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter

        ToolButton {
            id: buttonOpen
            Layout.minimumWidth: 60
            Layout.minimumHeight: 60
            text: "\uf07c"
            font.family: "Font Awesome 6 Free"
            font.pixelSize: 20

            ToolTip.visible: hovered
            ToolTip.text: qsTr("Ouvrir")

            onClicked: openSomDialog.open()
        }

        ToolButton {
            id: buttonSave
            Layout.minimumWidth: 60
            Layout.minimumHeight: 60
            text: "\uf0c7"
            font.family: "Font Awesome 6 Free"
            font.pixelSize: 20

            ToolTip.visible: hovered
            ToolTip.text: qsTr("Sauvegarder")

            onClicked: saveSomDialog.open()
        }

        ToolButton {
            id: buttonImport
            Layout.minimumWidth: 60
            Layout.minimumHeight: 60
            text: "\uf19d"
            font.family: "Font Awesome 6 Free"
            font.pixelSize: 20

            ToolTip.visible: hovered
            ToolTip.text: qsTr("Entrainer un jeu de données")

            onClicked: trainDialog.open()
        }

        ToolButton {
            id: buttonAnalyze
            Layout.minimumWidth: 60
            Layout.minimumHeight: 60
            text: "\uf2db"
            font.family: "Font Awesome 6 Free"
            font.pixelSize: 20

            ToolTip.visible: hovered
            ToolTip.text: qsTr("Analyser un jeu de données")

            onClicked: analyzeDialog.open()
        }
    }

    FileDialog {
        id: openSomDialog
        fileMode: FileDialog.OpenFile
        nameFilters: ["Fichiers SOM (*.som)"]
        onAccepted: openFile(file)
    }

    FileDialog {
        id: saveSomDialog
        fileMode: FileDialog.SaveFile
        nameFilters: ["Fichiers SOM (*.som)"]
        onAccepted: saveFile(file)
    }

    FileDialog {
        id: trainDialog
        fileMode: FileDialog.OpenFile
        nameFilters: ["Fichiers CSV (*.csv)", "Fichiers texte (*.txt)"]
        onAccepted: trainFile(file)
    }

    FileDialog {
        id: analyzeDialog
        fileMode: FileDialog.OpenFile
        nameFilters: ["Fichiers CSV (*.csv)", "Fichiers texte (*.txt)"]
        onAccepted: analyzeFile(file)
    }
}
