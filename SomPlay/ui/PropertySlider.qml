import QtQuick 2.12
import QtQuick.Controls.Fusion 2.12
import QtQuick.Layouts 6.5

ColumnLayout {
    id: propertySlider

    signal valueChanged()
    property alias currentValue: slider.value

    property string label: '{}'
    property string valueFormat: '{}'
    property double min: 0
    property double max: 1
    property double step: 0.1

    Layout.fillWidth: true

    Label {
        id: sliderLabel
        text: label + ": " + valueFormat.replace("{}", +slider.value)
    }

    RowLayout {
        Layout.fillWidth: true

        Label {
            text: min
            color: "#838383"
        }

        Slider {
            id: slider
            Layout.fillWidth: true
            from: min
            to: max
            value: min
            snapMode: Slider.SnapAlways
            stepSize: step
            onMoved:  {
                sliderLabel.text = label + ": " + valueFormat.replace("{}", Math.round(slider.value * 100) / 100);
                propertySlider.valueChanged()
            }
        }

        Label {
            text: max
            color: "#838383"
            Layout.alignment: Qt.AlignRight
        }
    }
}
