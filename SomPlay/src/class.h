/*
 * @file         class.h
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-05-03
 */

#ifndef SOM_CLASS_H
#define SOM_CLASS_H

#include <QObject>
#include <QColor>

/**
 *
 */
class SomClass : public QObject {
Q_OBJECT
  Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
  Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)

public:
  explicit SomClass(QObject *parent = nullptr);
  explicit SomClass(const QString &name, QColor color, QObject *parent = nullptr);

  [[nodiscard]] QString name() const;
  void setName(const QString &name);

  [[nodiscard]] QColor color() const;
  void setColor(QColor color);

signals:
  void nameChanged();
  void colorChanged();

private:
  QString name_;
  QColor color_;
};


#endif // SOM_CLASS_H
