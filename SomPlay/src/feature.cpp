/*
 * @file         feature.cpp
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-05-03
 */


#include <QObject>
#include "feature.h"

/**
 *
 * @param parent
 */
SomFeature::SomFeature(QObject *parent)
    : QObject(parent) { }

/**
 *
 * @param name
 * @param index
 * @param parent
 */
SomFeature::SomFeature(const QString &name, int index, QObject *parent)
: QObject(parent), name_(name), index_(index) {}

/**
 *
 * @return
 */
QString SomFeature::name() const {
  return name_;
}

/**
 *
 * @param name
 */
void SomFeature::setName(const QString &name) {
  if (name_ != name) {
    name_ = name;
    emit nameChanged();
  }
}

/**
 *
 * @return
 */
int SomFeature::index() const {
  return index_;
}

/**
 *
 * @param index
 */
void SomFeature::setIndex(int index) {
  if (index_ != index) {
    index_ = index;
    emit indexChanged();
  }
}
