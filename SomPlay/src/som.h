/*
 * @file         com.h
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-05-03
 */

#ifndef SOM_H
#define SOM_H

#include <QObject>
#include <QQuickPaintedItem>
#include <QThread>
#include <QTimer>
#include "somlib.h"
#include "worker.h"
#include "feature.h"

/**
 *
 */
class SelfOrganizingMap: public QQuickPaintedItem {
  Q_OBJECT
  Q_PROPERTY(int mapWidth MEMBER width_)
  Q_PROPERTY(int mapHeight MEMBER height_)
  Q_PROPERTY(int iterations MEMBER iterations_)
  Q_PROPERTY(int radius0 MEMBER radius0_)
  Q_PROPERTY(int sigma0 MEMBER sigma0_)
  Q_PROPERTY(double alpha0 MEMBER alpha0_)
  Q_PROPERTY(INIT_MODE initMode MEMBER init_mode_)
  Q_PROPERTY(double initMin MEMBER min_)
  Q_PROPERTY(double initMax MEMBER max_)
  Q_PROPERTY(DECAY_MODE radiusDecay MEMBER radius_decay_)
  Q_PROPERTY(DECAY_MODE sigmaDecay MEMBER sigma_decay_)
  Q_PROPERTY(DECAY_MODE alphaDecay MEMBER alpha_decay_)
  Q_PROPERTY(NEIGHBORHOOD_FUNCTION neighborhoodFunction MEMBER neighborhood_function_)
  Q_PROPERTY(PALETTE palette MEMBER palette_)
  Q_PROPERTY(bool invertPalette MEMBER invert_palette_)
  Q_PROPERTY(bool displayTrainingWinners MEMBER display_training_winners_)
  Q_PROPERTY(bool displayAnalyzingWinners MEMBER display_analyzing_winners_)
  Q_PROPERTY(QQmlListProperty<SomClass> classes READ classes NOTIFY classesChanged)
  Q_PROPERTY(QQmlListProperty<SomFeature> features READ features NOTIFY featuresChanged)
  Q_PROPERTY(int displayFeature MEMBER display_feature_ WRITE setDisplayFeature NOTIFY displayFeatureChanged)
  Q_PROPERTY(bool loaded READ loaded NOTIFY loadedChanged)

public:
  explicit SelfOrganizingMap(QQuickItem *parent = nullptr);
  ~SelfOrganizingMap() override;

  void paint(QPainter *painter) override;

  Q_ENUM(INIT_MODE)
  Q_ENUM(DECAY_MODE)
  Q_ENUM(NEIGHBORHOOD_FUNCTION)
  Q_ENUM(PALETTE)

  QQmlListProperty<SomClass> classes();
  static void appendClass(QQmlListProperty<SomClass>* list, SomClass* item);
  static qsizetype classesCount(QQmlListProperty<SomClass>* list);
  static SomClass* classAt(QQmlListProperty<SomClass>* list, qsizetype index);

  QQmlListProperty<SomFeature> features();
  static void appendFeature(QQmlListProperty<SomFeature>* list, SomFeature* item);
  static qsizetype featuresCount(QQmlListProperty<SomFeature>* list);
  static SomFeature* featureAt(QQmlListProperty<SomFeature>* list, qsizetype index);

  [[nodiscard]] bool loaded() const { return loaded_; }

public slots:
  void fileLoaded(
      std::shared_ptr<const Input> input,
      std::shared_ptr<const Map> map,
      std::shared_ptr<const UMatrix> umatrix,
      std::shared_ptr<const Winners> winners
  );
  void trainFile(const QUrl &url);
  void analyzeFile(const QUrl &url);
  void refreshTraining();
  void refreshDisplay(QString message, int step, int steps, bool force = false);
  void refreshDisplay();
  void trainingInputLoaded(std::shared_ptr<const Input> input);
  void trainingProgress(
      QString message, int step, int steps,
      std::shared_ptr<const Map> map,
      std::shared_ptr<const UMatrix> umatrix,
      std::shared_ptr<const Winners> winners
  );
  void trainingFinished(QString message);
  void analyzingInputLoaded(std::shared_ptr<const Input> input);
  void analyzingFinished(std::shared_ptr<const Winners> winners);

signals:
  void openFile(const QUrl &url);
  void saveFile(const QUrl &url) const;
  void sendStartTraining(QString filename, const TrainingParameters &parameters);
  void sendStartAnalyzing(QString filename);
  void sendContinueTraining();
  void sendRefreshTraining(const TrainingParameters &parameters);
  void sendContinuAnalyzing();
  void error(QString error);

  void classesChanged();
  void featuresChanged();
  void displayFeatureChanged();
  void progress(QString message, int step, int steps);
  void finished();
  void loadedChanged();

private:
  void startWorker();
  void killWorker();

  void computeClasses();
  void computeFeatures();
  [[nodiscard]] int computeZoomFactor(int nb_lines, int nb_columns) const;
  void computeZoomAndMargins(int nb_lines, int nb_columns, int &zoom, int &x_margin, int &y_margin) const;
  [[nodiscard]] PALETTE getPalette() const;
  void setDisplayFeature(int feature);

  void paintUMatrix(QPainter *painter) const;
  void paintFeature(QPainter *painter) const;
  void paintUMatrixWinners(QPainter *painter, const Winners *winners, bool class_color) const;
  void paintMapWinners(QPainter *painter, const Winners *winners, bool class_color) const;
  void updateTimeout();

  int width_{10};
  int height_{10};
  int iterations_{500};
  int radius0_{100}; // %
  int sigma0_{100}; // %
  double alpha0_{0.9};
  double min_{0.0};
  double max_{0.0};
  INIT_MODE init_mode_{INIT_MODE_RANDOM};
  DECAY_MODE radius_decay_{DECAY_MODE_LINEAR};
  DECAY_MODE sigma_decay_{DECAY_MODE_LINEAR};
  DECAY_MODE alpha_decay_{DECAY_MODE_LINEAR};
  NEIGHBORHOOD_FUNCTION neighborhood_function_{NEIGHBORHOOD_FUNCTION_BUBBLE};
  PALETTE palette_{PALETTE_VIRIDIS};
  bool invert_palette_{false};
  bool display_training_winners_{true};
  bool display_analyzing_winners_{true};
  int display_feature_ = -1; // -1 for UMatrix

  QThread thread_;
  QList<SomClass*> classes_;
  QList<SomFeature*> features_;
  QTimer timer_;
  QTime last_refresh_{QTime::currentTime()};
  QString message_{""};
  int step_{0};
  int steps_{0};
  bool loaded_{false};

  std::shared_ptr<const Input> training_input_;
  std::shared_ptr<const Input> analyzing_input_;
  std::shared_ptr<const Map> map_;
  std::shared_ptr<const UMatrix> umatrix_;
  std::shared_ptr<const Winners> training_winners_;
  std::shared_ptr<const Winners> analyzing_winners_;
};


#endif // SOM_H
