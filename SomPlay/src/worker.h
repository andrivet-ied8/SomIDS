/*
 * @file         worker.h
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-05-03
 */

#ifndef WORKER_H
#define WORKER_H

#include <QObject>
#include "somlib.h"
#include "class.h"
#include "feature.h"

/**
 *
 */
struct TrainingParameters {
  int width{10}; //< Largeur de la carte
  int height{10}; //< Hauteur de la carte
  int iterations{500}; //< Nombre d'itérations.
  int radius0{100}; //< Rayon du voisinage initial (%).
  int sigma0{100}; //< Ecart type initial (%).
  double alpha0{0.9}; //< Coefficient (taux) d'apprentissage initial.
  INIT_MODE initMode{INIT_MODE_RANDOM}; //< Mode d'initialisation des poids des neurones.
  double initMin{0.0}; //< Minimum a retrancher de la moyenne
  double initMax{0.0}; //< Maximum a ajouter à la moyenne
  DECAY_MODE radius_decay; //< Mode d'évolution du rayon/sigma
  DECAY_MODE sigma_decay; //< Mode d'évolution du rayon/sigma
  DECAY_MODE alpha_decay; //< Mode d'évolution du facteur d'apprentissage (alpha)
  NEIGHBORHOOD_FUNCTION neighborhood_function; //< Fonction de voisinage
};

/**
 *
 */
struct Deleters {
  inline void operator()(const Training *training) const { training_destroy(training); }
  inline void operator()(const Analyzing *analyzing) const { analyzing_destroy(analyzing); }
  inline void operator()(const Input *input) const { input_destroy(input); }
  inline void operator()(const Map *map) const { map_destroy(map); }
  inline void operator()(const Winners *winners) const { winners_destroy(winners); }
  inline void operator()(const UMatrix *umatrix) const { umatrix_destroy(umatrix); }
};

Q_DECLARE_METATYPE(std::shared_ptr<const Input>)
Q_DECLARE_METATYPE(std::shared_ptr<const Map>)

/**
 *
 */
class Worker : public QObject
{
Q_OBJECT

public:
  Worker() = default;
  ~Worker() override = default;

public slots:
  void openFile(const QUrl &url);
  void saveFile(const QUrl &url) const;
  void startTraining(QString filename, const TrainingParameters &parameters);
  void continueTraining();
  void refreshTraining(const TrainingParameters &parameters);
  void startAnalyzing(QString filename);
  void continuAnalyzing();

signals:
  void fileLoaded(
      std::shared_ptr<const Input> input,
      std::shared_ptr<const Map> map,
      std::shared_ptr<const UMatrix> umatrix,
      std::shared_ptr<const Winners> winners
  );
  void trainingInputLoaded(std::shared_ptr<const Input> input);
  void trainingProgress(
    QString message, int step, int steps,
    std::shared_ptr<const Map> map,
    std::shared_ptr<const UMatrix> umatrix,
    std::shared_ptr<const Winners> winners
  );
  void trainingFinished(QString message);
  void analyzingInputLoaded(std::shared_ptr<const Input> input);
  void error(QString error);
  void analyzingFinished(std::shared_ptr<const Winners> winners);

private:
  [[nodiscard]] int getScaledRadius(int radius) const;

  TrainingParameters parameters_;
  int iteration_ = 0;
  QTime start_time_;
  std::shared_ptr<const Input> training_input_;
  std::shared_ptr<const Input> analyzing_input_;
  std::shared_ptr<const Map> map_;
  std::shared_ptr<const UMatrix> umatrix_;
  std::shared_ptr<const Winners> training_winners_;
  std::shared_ptr<const Winners> analyzing_winners_;
  std::unique_ptr<Analyzing, Deleters> analyzing_;
  std::unique_ptr<Training, Deleters> training_;
};



#endif // WORKER_H
