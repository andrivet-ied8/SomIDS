/*
 * @file         scale.cpp
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-05-03
 */

#include "QPainter"
#include "scale.h"

constexpr double Y_INCREMENT = 4;

/**
 *
 * @param painter
 */
void Scale::paint(QPainter *painter) {
  painter->setPen(Qt::NoPen);

  unsigned char r, g, b;
  PALETTE palette = getPalette();

  auto y = boundingRect().top();
  while(y < boundingRect().bottom()) {
    double value = (y - boundingRect().top()) / boundingRect().height();
    colormap_convert(1.0 - value, palette, &r, &g, &b);
    painter->setBrush(QColor(r, g, b));
    painter->drawRect(
        static_cast<int>(boundingRect().left()),
        static_cast<int>(y),
        static_cast<int>(boundingRect().width()),
        static_cast<int>(Y_INCREMENT)
    );
    y += Y_INCREMENT;
  }
}

/**
 *
 * @return
 */
PALETTE Scale::getPalette() const {
  PALETTE palette = palette_;
  if(invert_palette_) palette = static_cast<PALETTE>(palette | PALETTE_INVERT);
  return palette;
}

/**
 *
 */
void Scale::paletteChanged() {
  update();
}