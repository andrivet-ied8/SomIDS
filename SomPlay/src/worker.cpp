/*
 * @file         worker.cpp
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-05-03
 */

#include <QUrl>
#include "som.h"

/**
 *
 * @param url
 */
void Worker::openFile(const QUrl &url) {
  auto input = std::make_unique<Input>();
  auto map = std::make_unique<Map>();
  auto winners = std::make_unique<Winners>();

  QString filename = url.toLocalFile();
  QByteArray filename_array = filename.toLatin1();
  if(!data_load(input.get(), map.get(), winners.get(), filename_array.constData())) return;

  std::unique_ptr<const UMatrix> umatrix(umatrix_generate(map.get()));
  if(!umatrix) return;

  training_input_ = std::shared_ptr<const Input>(input.release(), input.get_deleter());
  analyzing_input_.reset();
  map_ = std::shared_ptr<const Map>(map.release(), map.get_deleter());
  umatrix_ = std::shared_ptr<const UMatrix>(umatrix.release(), umatrix.get_deleter());
  training_winners_ = std::shared_ptr<Winners>(winners.release(), winners.get_deleter());
  analyzing_winners_.reset();

  emit fileLoaded(training_input_, map_, umatrix_, training_winners_);
}

/**
 *
 * @param url
 */
void Worker::saveFile(const QUrl &url) const {
  QString filename = url.toLocalFile();
  QByteArray filename_array = filename.toLatin1();
  data_save(training_input_.get(), map_.get(), training_winners_.get(), filename_array.constData());
}

/**
 *
 * @param filename
 * @param parameters
 */
void Worker::startTraining(QString filename, const TrainingParameters &parameters) {
  QByteArray byteArray = filename.toLatin1();
  std::unique_ptr<Input, Deleters> input{input_read(byteArray.constData())};
  if(!input) return;

  input_normalize(input.get());

  training_input_ = std::shared_ptr<const Input>(input.release(), input.get_deleter());
  emit trainingInputLoaded(training_input_);
  refreshTraining(parameters);
}

/**
 *
 */
void Worker::continueTraining() {
  if(!training_) return;

  ++iteration_;
  std::unique_ptr<Winners, Deleters> winner{winners_init(training_input_->nb_samples)};
  std::unique_ptr<Map, Deleters> map(map_copy(map_.get()));
  if(!winner || !map) return;

  if(training_next(map.get(), training_.get(), winner.get(), training_input_.get())) {
    map_classify(map.get(), training_input_.get());
    std::unique_ptr<UMatrix, Deleters> umatrix{umatrix_generate(map.get())};
    if(!umatrix) return;

    map_ = std::shared_ptr<const Map>(map.release(), map.get_deleter());
    umatrix_ = std::shared_ptr<const UMatrix>(umatrix.release(), umatrix.get_deleter());
    training_winners_ = std::shared_ptr<const Winners>(winner.release(), winner.get_deleter());

    auto message = QString("Classes: %1, Dimensions: %2, Echantillons: %3, Itération %4 / %5")
        .arg(training_input_->nb_classes).arg(training_input_->nb_dimensions).arg(training_input_->nb_samples)
        .arg(iteration_).arg(parameters_.iterations);

    emit trainingProgress(
      message,
      iteration_, parameters_.iterations,
      map_,
      umatrix_,
      training_winners_
    );
  }
  else {
    int elapsedSeconds = start_time_.secsTo(QTime::currentTime());
    int hours = elapsedSeconds / 3600;
    int minutes = (elapsedSeconds % 3600) / 60;
    int seconds = elapsedSeconds % 60;

    auto message = QString("Classes: %1, Dimensions: %2, Echantillons: %3, Itérations %4, Time: %5:%6:%7")
        .arg(training_input_->nb_classes).arg(training_input_->nb_dimensions).arg(training_input_->nb_samples)
        .arg(parameters_.iterations)
        .arg(hours, 2, 10, QLatin1Char('0')).arg(minutes, 2, 10, QLatin1Char('0')).arg(seconds, 2, 10, QLatin1Char('0'));
    emit trainingFinished(message);
  }
}

/**
 *
 * @return
 */
int Worker::getScaledRadius(int percent) const {
  return percent * std::max(parameters_.width, parameters_.height) / 100;
}

/**
 *
 * @param parameters
 */
void Worker::refreshTraining(const TrainingParameters &parameters) {
  parameters_ = parameters;

  const double *means =
      (parameters_.initMode == INIT_MODE_AVERAGE) ?
        vector_compute_means(training_input_->values, training_input_->nb_dimensions, training_input_->nb_samples) :
        nullptr;

  std::unique_ptr<Map, Deleters> map{map_generate(
      parameters.height, parameters.width,
      training_input_->nb_dimensions,
      parameters.initMode, means, parameters.initMin, parameters.initMax)};
  if(!map) return;

  int radius0 = getScaledRadius(parameters_.radius0);
  int sigma0 = getScaledRadius(parameters_.sigma0);

  training_.reset(training_init(
      training_input_->nb_samples, map_nb_neurones(map.get()), parameters.iterations,
      radius0, sigma0, parameters.alpha0,
      parameters.radius_decay, parameters.sigma_decay, parameters.alpha_decay, parameters.neighborhood_function
  ));
  if(!training_) return;

  map_ = std::shared_ptr<const Map>(map.release(), map.get_deleter());

  iteration_ = 0;
  start_time_ = QTime::currentTime();
  emit trainingProgress("Training started", 0, parameters.iterations,  map_, nullptr, nullptr);
}

/**
 *
 * @param filename
 */
void Worker::startAnalyzing(QString filename) {
  if(!training_input_) {
    emit error("Please load a file for the training before starting an analysis");
    emit analyzingFinished(nullptr);
    return;
  }

  QByteArray byteArray = filename.toLatin1();
  std::unique_ptr<Input, Deleters> input(input_read(byteArray.constData()));
  if(!input) {
    emit error("Error loading file");
    emit analyzingFinished(nullptr);
    return;
  }

  if(!input_is_compatible(training_input_.get(), input.get())) {
    emit error("This file is not compatible with the current map");
    emit analyzingFinished(nullptr);
    return;
  }

  input_normalize(input.get());

  int radius0 = getScaledRadius(parameters_.radius0);
  int sigma0 = getScaledRadius(parameters_.sigma0);

  auto analyzing = std::unique_ptr<Analyzing, Deleters>(analyzing_init(
      input->nb_samples, map_nb_neurones(map_.get()),
      radius0, sigma0, parameters_.alpha0
  ));
  if(!analyzing) {
    emit error("Error analyzing file");
    emit analyzingFinished(nullptr);
    return;
  }

  analyzing_.reset(analyzing.release());
  analyzing_input_ = std::shared_ptr<const Input>(input.release(), input.get_deleter());
  emit analyzingInputLoaded(training_input_);
}

/**
 *
 */
void Worker::continuAnalyzing() {
  std::unique_ptr<Winners, Deleters> winners{winners_init(analyzing_input_->nb_samples)};
  analyzing(map_.get(), analyzing_.get(), winners.get(), analyzing_input_.get());
  emit analyzingFinished(std::shared_ptr<const Winners>(winners.release(), winners.get_deleter()));
}
