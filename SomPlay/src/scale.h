/*
 * @file         scale.h
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-05-03
 */
#ifndef SCALE_H
#define SCALE_H

#include <QQuickPaintedItem>
#include "somlib.h"

/**
 *
 */
class Scale: public QQuickPaintedItem {
  Q_OBJECT
  Q_PROPERTY(PALETTE palette MEMBER palette_ NOTIFY paletteChanged)
  Q_PROPERTY(bool invertPalette MEMBER invert_palette_ NOTIFY paletteChanged)

public:
  explicit Scale(QQuickItem *parent = nullptr): QQuickPaintedItem(parent) {}
  ~Scale() override = default;

  void paint(QPainter *painter) override;

public slots:
  void paletteChanged();

private:
  [[nodiscard]] PALETTE getPalette() const;

  PALETTE palette_{PALETTE_VIRIDIS};
  bool invert_palette_{false};
};


#endif // SCALE_H
