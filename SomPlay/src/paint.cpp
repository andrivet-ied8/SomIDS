/*
 * @file         paint.c
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-05-03
 */

#include "QPainter"
#include "som.h"

static constexpr int TRAINING_MARK_SIZE = 4;
static constexpr int ANALYSIS_MARK_SIZE = 8;
static constexpr int REFRESH_DELAY = 250; // ms

/**
 * Rafraichit l'affichage.
 */
void SelfOrganizingMap::refreshDisplay(QString message, int step, int steps, bool force) {
  if(!force && last_refresh_.addMSecs(REFRESH_DELAY) > QTime::currentTime()) {
    message_ = std::move(message);
    step_ = step;
    steps_ = steps;
    timer_.start(REFRESH_DELAY);
    return;
  }

  update();
  emit progress(message, step, steps);
}

void SelfOrganizingMap::refreshDisplay() {
  update();
}

/**
 * Temp mort pour le minuteur de rafraichissement.
 */
void SelfOrganizingMap::updateTimeout() {
  update();
  emit progress(message_, step_, steps_);
}

/**
 * Dessine le graphique.
 *
 * @param painter
 */
void SelfOrganizingMap::paint(QPainter *painter) {
  last_refresh_ = QTime::currentTime();
  painter->setPen(Qt::NoPen);
  painter->setBrush(Qt::black);
  painter->drawRect(boundingRect());

  if(display_feature_ < 0)
    paintUMatrix(painter);
  else
    paintFeature(painter);
}

/**
 * Obtient la palette courante avec l'éventuelle inversion.
 *
 * @return La palette courante avec l'éventuelle inversion.
 */
PALETTE SelfOrganizingMap::getPalette() const {
  PALETTE palette = palette_;
  if(invert_palette_) palette = static_cast<PALETTE>(palette | PALETTE_INVERT);
  return palette;
}

/**
 * Calcul le facteur de zoom en fonction de la taille.
 *
 * @param nb_lines Nombre de lignes de ma carte.
 * @param nb_columns Nombre de colonnes de la carte.
 * @return Le facteur de zoom.
 */
int SelfOrganizingMap::computeZoomFactor(int nb_lines, int nb_columns) const {
  auto x_factor = static_cast<int>(boundingRect().width() /  nb_columns);
  auto y_factor = static_cast<int>(boundingRect().height() /  nb_lines);
  if(x_factor < 1) x_factor = 1;
  if(y_factor < 1) y_factor = 1;
  return std::min(x_factor, y_factor);
}

/**
 * Calcul le facteur de zoom et les marges.
 *
 * @param nb_lines Nombre de lignes de ma carte.
 * @param nb_columns Nombre de colonnes de la carte.
 * @param zoom Le facteur de zoom.
 * @param x_margin Les marges horizontales.
 * @param y_margin Les marges verticales.
 */
void SelfOrganizingMap::computeZoomAndMargins(int nb_lines, int nb_columns, int &zoom, int &x_margin, int &y_margin) const {
  zoom = computeZoomFactor(nb_lines, nb_columns);
  x_margin = static_cast<int>(boundingRect().width() - nb_columns * zoom) / 2;
  y_margin = static_cast<int>(boundingRect().height() - nb_lines * zoom) / 2;
}

/**
 * Dessine un U-matrice.
 *
 * @param painter
 */
void SelfOrganizingMap::paintUMatrix(QPainter *painter) const {
  if(!umatrix_) return;

  int factor, x_margin, y_margin;
  computeZoomAndMargins(umatrix_->nb_lines, umatrix_->nb_columns, factor, x_margin, y_margin);

  unsigned char r, g, b;
  PALETTE palette = getPalette();

  const double *weights = umatrix_->weights;
  for(int l = 0; l < umatrix_->nb_lines; l++) {
    for(int c = 0; c < umatrix_->nb_columns; c++, ++weights) {
      colormap_convert(*weights, palette, &r, &g, &b);
      painter->setBrush(QColor(r, g, b));
      painter->drawRect(x_margin + c * factor, y_margin + l * factor, factor, factor);
    }
  }

  if(display_training_winners_)
    paintUMatrixWinners(painter, training_winners_.get(), true);

  if(display_analyzing_winners_)
    paintUMatrixWinners(painter, analyzing_winners_.get(), false);
}

/**
 * Dessine les gagnants (BMU) pour une U-matrice.
 *
 * @param painter
 * @param winners Les gagnants.
 * @param class_color La couleur à utiliser.
 */
void SelfOrganizingMap::paintUMatrixWinners(QPainter *painter, const Winners *winners, bool class_color) const {
  if(!winners) return;

  if(class_color)
    painter->setPen(Qt::NoPen);
  else {
    painter->setPen(QColorConstants::White);
    painter->setBrush(Qt::NoBrush);
  }

  int factor, x_margin, y_margin;
  computeZoomAndMargins(umatrix_->nb_lines, umatrix_->nb_columns, factor, x_margin, y_margin);

  for(int i = 0; i < winners->nb_winners; ++i) {
    int bmu = winners->neurons[i];
    int l = bmu / umatrix_->map_nb_columns;
    int c = bmu % umatrix_->map_nb_columns;
    int umatrix_l = l * umatrix_->nb_lines / umatrix_->map_nb_lines;
    int umatrix_c = c * umatrix_->nb_columns / umatrix_->map_nb_columns;

    if(class_color) {
      int cls_index = map_->neurons[bmu].class_index;
      const SomClass *cls = classes_[cls_index];
      painter->setBrush(cls->color());

      int mark_margin = (factor - TRAINING_MARK_SIZE) / 2;
      painter->drawRect(x_margin + mark_margin + umatrix_c * factor, y_margin + mark_margin + umatrix_l * factor, TRAINING_MARK_SIZE, TRAINING_MARK_SIZE);
    }
    else {
      int mark_margin = (factor - ANALYSIS_MARK_SIZE) / 2;
      painter->drawRect(x_margin + mark_margin + umatrix_c * factor, y_margin + mark_margin + umatrix_l * factor, ANALYSIS_MARK_SIZE, ANALYSIS_MARK_SIZE);
    }
  }
}

/**
 * Dessine les gagnants (BMU) pour une carte.
 *
 * @param painter
 * @param winners Les gagnants.
 * @param class_color La couleur à utiliser.
 */
void SelfOrganizingMap::paintMapWinners(QPainter *painter, const Winners *winners, bool class_color) const {
  if(!winners) return;

  if(class_color)
    painter->setPen(Qt::NoPen);
  else {
    painter->setPen(QColorConstants::White);
    painter->setBrush(Qt::NoBrush);
  }

  int factor, x_margin, y_margin;
  computeZoomAndMargins(map_->nb_lines, map_->nb_columns, factor, x_margin, y_margin);

  for(int i = 0; i < winners->nb_winners; ++i) {
    int bmu = winners->neurons[i];
    int l = bmu / map_->nb_columns;
    int c = bmu % map_->nb_columns;

    if(class_color) {
      int cls_index = map_->neurons[bmu].class_index;
      const SomClass *cls = classes_[cls_index];
      painter->setBrush(cls->color());

      int mark_margin = (factor - TRAINING_MARK_SIZE) / 2;
      painter->drawRect(x_margin + mark_margin + c * factor, y_margin + mark_margin + l * factor, TRAINING_MARK_SIZE, TRAINING_MARK_SIZE);
    }
    else {
      int mark_margin = (factor - ANALYSIS_MARK_SIZE) / 2;
      painter->drawRect(x_margin + mark_margin + c * factor, y_margin + mark_margin + l * factor, ANALYSIS_MARK_SIZE, ANALYSIS_MARK_SIZE);
    }
  }
}

/**
 *
 * @param painter
 */
void SelfOrganizingMap::paintFeature(QPainter *painter) const {
  if(!map_) return;

  auto factor = computeZoomFactor(map_->nb_lines, map_->nb_columns);
  auto x_margin = static_cast<int>(boundingRect().width() - map_->nb_columns * factor) / 2;
  auto y_margin = static_cast<int>(boundingRect().height() - map_->nb_lines * factor) / 2;

  unsigned char r, g, b;
  PALETTE palette = getPalette();

  auto const *neuron = map_->neurons;
  for(int l = 0; l < map_->nb_lines; l++) {
    for(int c = 0; c < map_->nb_columns; c++, ++neuron) {
      auto weight = neuron->weights[display_feature_];
      colormap_convert(weight, palette, &r, &g, &b);
      painter->setBrush(QColor(r, g, b));
      painter->drawRect(x_margin + c * factor, y_margin + l * factor, factor, factor);
    }
  }

  if(display_training_winners_)
    paintMapWinners(painter, training_winners_.get(), true);

  if(display_analyzing_winners_)
    paintMapWinners(painter, analyzing_winners_.get(), false);
}
