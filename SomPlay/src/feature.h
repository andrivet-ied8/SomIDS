/*
 * @file         feature.h
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-05-03
 */

#ifndef SOM_FEATURE_H
#define SOM_FEATURE_H

#include <QObject>

/**
 *
 */
class SomFeature: public QObject {
Q_OBJECT
  Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
  Q_PROPERTY(int index READ index WRITE setIndex NOTIFY indexChanged)

public:
  explicit SomFeature(QObject *parent = nullptr);
  explicit SomFeature(const QString &name, int index, QObject *parent = nullptr);

  [[nodiscard]] QString name() const;
  void setName(const QString &name);

  [[nodiscard]] int index() const;
  void setIndex(int index);

signals:
  void nameChanged();
  void indexChanged();

private:
  QString name_{""};
  int index_{0};
};


#endif // SOM_FEATURE_H
