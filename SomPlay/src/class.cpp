/*
 * @file         class.cpp
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-05-03
 */


#include <QObject>
#include "class.h"

/**
 *
 * @param parent
 */
SomClass::SomClass(QObject *parent)
: QObject(parent) { }

/**
 *
 * @param name
 * @param color
 * @param parent
 */
SomClass::SomClass(const QString &name, QColor color, QObject *parent)
: QObject(parent), name_(name), color_(color) {}

/**
 *
 * @return
 */
QString SomClass::name() const {
  return name_;
}

/**
 *
 * @param name
 */
void SomClass::setName(const QString &name) {
  if (name_ != name) {
    name_ = name;
    emit nameChanged();
  }
}

/**
 *
 * @return
 */
QColor SomClass::color() const {
  return color_;
}

/**
 *
 * @param color
 */
void SomClass::setColor(QColor color) {
  if (color_ != color) {
    color_ = color;
    emit colorChanged();
  }
}
