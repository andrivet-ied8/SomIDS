/*
 * @file         main.cpp
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-05-03
 */

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QFontDatabase>
#include "som.h"
#include "scale.h"

/**
 *
 * @param name
 */
void LoadFont(const QString &name) {
  int fontId = QFontDatabase::addApplicationFont(name);
  if ( fontId != -1) {
    const QString fontFamily = QFontDatabase::applicationFontFamilies(fontId).at(0);
    qDebug() << "Loaded font:" << fontFamily;
  } else {
    qDebug() << "Failed to load font";
  }
}

/**
 *
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char *argv[])
{
  QGuiApplication app(argc, argv);

  LoadFont(":/Font Awesome 6 Free-Solid");

  qmlRegisterType<SelfOrganizingMap>("com.andrivet.somplay", 1, 0, "SelfOrganizingMap");
  qmlRegisterType<Scale>("com.andrivet.somplay", 1, 0, "SomScale");
  qRegisterMetaType<std::shared_ptr<const Input>>("std::shared_ptr<const Input>");
  qRegisterMetaType<std::shared_ptr<const Map>>("std::shared_ptr<const Map>");

  QQmlApplicationEngine engine;
  QObject::connect(
      &engine,
      &QQmlApplicationEngine::objectCreationFailed,
      &app,
      []() { QCoreApplication::exit(-1); },
      Qt::QueuedConnection);
  const QUrl url(QStringLiteral("qrc:/Main.qml"));
  engine.load(url);

  return QGuiApplication::exec();
}
