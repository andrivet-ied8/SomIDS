/*
 * @file         som.cpp
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-05-03
 */

#include <QUrl>
#include <utility>
#include "som.h"

/**
 *
 * @param parent
 */
SelfOrganizingMap::SelfOrganizingMap(QQuickItem *parent)
: QQuickPaintedItem(parent) {
  timer_.setSingleShot(true);
  connect(&timer_, &QTimer::timeout, this, &SelfOrganizingMap::updateTimeout);
  startWorker();
}

/**
 *
 */
SelfOrganizingMap::~SelfOrganizingMap() {
  timer_.stop();
  killWorker();
}

/**
 *
 * @return
 */
QQmlListProperty<SomClass> SelfOrganizingMap::classes() {
  return {
    this, this,
    &SelfOrganizingMap::appendClass,
    &SelfOrganizingMap::classesCount,
    &SelfOrganizingMap::classAt,
    nullptr};
}

/**
 *
 * @param list
 * @param item
 */
void SelfOrganizingMap::appendClass(QQmlListProperty<SomClass>* list, SomClass* item) {
  auto *container = qobject_cast<SelfOrganizingMap*>(list->object);
  if (container) {
    item->setParent(container);
    container->classes_.append(item);
    emit container->classesChanged();
  }
}

/**
 *
 * @param list
 * @return
 */
qsizetype SelfOrganizingMap::classesCount(QQmlListProperty<SomClass>* list) {
  auto const *container = qobject_cast<SelfOrganizingMap *>(list->object);
  return container ? container->classes_.size() : 0;
}

/**
 *
 * @param list
 * @param index
 * @return
 */
SomClass* SelfOrganizingMap::classAt(QQmlListProperty<SomClass>* list, qsizetype index) {
  SelfOrganizingMap const *container = qobject_cast<SelfOrganizingMap *>(list->object);
  if (container && index >= 0 && index < container->classes_.size()) {
    return container->classes_.at(index);
  }
  return nullptr;
}

/**
 *
 * @return
 */
QQmlListProperty<SomFeature> SelfOrganizingMap::features() {
  return {
      this, this,
      &SelfOrganizingMap::appendFeature,
      &SelfOrganizingMap::featuresCount,
      &SelfOrganizingMap::featureAt,
      nullptr};
}

/**
 *
 * @param list
 * @param item
 */
void SelfOrganizingMap::appendFeature(QQmlListProperty<SomFeature>* list, SomFeature* item) {
  auto *container = qobject_cast<SelfOrganizingMap*>(list->object);
  if (container) {
    item->setParent(container);
    container->features_.append(item);
    emit container->featuresChanged();
  }
}

/**
 *
 * @param list
 * @return
 */
qsizetype SelfOrganizingMap::featuresCount(QQmlListProperty<SomFeature>* list) {
  auto const *container = qobject_cast<SelfOrganizingMap *>(list->object);
  return container ? container->features_.size() : 0;
}

/**
 *
 * @param list
 * @param index
 * @return
 */
SomFeature* SelfOrganizingMap::featureAt(QQmlListProperty<SomFeature>* list, qsizetype index) {
  SelfOrganizingMap const *container = qobject_cast<SelfOrganizingMap *>(list->object);
  if (container && index >= 0 && index < container->features_.size()) {
    return container->features_.at(index);
  }
  return nullptr;
}

/**
 *
 */
void SelfOrganizingMap::startWorker() {
  auto *worker = new Worker();
  worker->moveToThread(&thread_);
  connect(&thread_, &QThread::finished, worker, &QObject::deleteLater);
  connect(this, &SelfOrganizingMap::openFile, worker, &Worker::openFile);
  connect(this, &SelfOrganizingMap::saveFile, worker, &Worker::saveFile);
  connect(this, &SelfOrganizingMap::sendStartTraining, worker, &Worker::startTraining);
  connect(this, &SelfOrganizingMap::sendContinueTraining, worker, &Worker::continueTraining);
  connect(this, &SelfOrganizingMap::sendRefreshTraining, worker, &Worker::refreshTraining);
  connect(this, &SelfOrganizingMap::sendStartAnalyzing, worker, &Worker::startAnalyzing);
  connect(this, &SelfOrganizingMap::sendContinuAnalyzing, worker, &Worker::continuAnalyzing);
  connect(worker, &Worker::fileLoaded, this, &SelfOrganizingMap::fileLoaded);
  connect(worker, &Worker::trainingInputLoaded, this, &SelfOrganizingMap::trainingInputLoaded);
  connect(worker, &Worker::trainingProgress, this, &SelfOrganizingMap::trainingProgress);
  connect(worker, &Worker::trainingFinished, this, &SelfOrganizingMap::trainingFinished);
  connect(worker, &Worker::analyzingInputLoaded, this, &SelfOrganizingMap::analyzingInputLoaded);
  connect(worker, &Worker::analyzingFinished, this, &SelfOrganizingMap::analyzingFinished);
  connect(worker, &Worker::error, this, &SelfOrganizingMap::error);
  thread_.start();
}

/**
 *
 */
void SelfOrganizingMap::killWorker() {
  thread_.quit();
  thread_.wait();
}

/**
 *
 */
void SelfOrganizingMap::computeClasses() {
  classes_.clear();
  const Class* cls = training_input_->classes;
  for(int i = 0; i < training_input_->nb_classes; ++i, ++cls)
    classes_.append(new SomClass(cls->name, QColor(cls->r, cls->g, cls->b)));
}

/**
 *
 */
void SelfOrganizingMap::computeFeatures() {
  features_.clear();
  for(int i = 0; i < training_input_->nb_dimensions; ++i)
    features_.append(new SomFeature(QString{"Feature #%1"}.arg(i + 1), i));
}

/**
 *
 * @param input
 * @param map
 * @param umatrix
 * @param winners
 */
void SelfOrganizingMap::fileLoaded(
    std::shared_ptr<const Input> input,
    std::shared_ptr<const Map> map,
    std::shared_ptr<const UMatrix> umatrix,
    std::shared_ptr<const Winners> winners
) {
  training_input_ = std::move(input);
  analyzing_input_.reset();
  map_ = std::move(map);
  umatrix_ = std::move(umatrix);
  training_winners_ = std::move(winners);
  analyzing_winners_.reset();

  computeClasses();
  computeFeatures();

  emit classesChanged();
  emit featuresChanged();
  refreshDisplay("File loaded", 0, 0, true);
}

/**
 *
 * @param url
 */
void SelfOrganizingMap::trainFile(const QUrl &url) {
  auto parameters = TrainingParameters {
    .width = width_,
    .height = height_,
    .iterations = iterations_,
    .radius0 = radius0_,
    .sigma0 = sigma0_,
    .alpha0 = alpha0_,
    .initMode = init_mode_,
    .initMin = min_,
    .initMax = max_
  };

  emit sendStartTraining(url.toLocalFile(), parameters);
}

/**
 *
 */
void SelfOrganizingMap::refreshTraining() {
  if(!training_input_) {
    emit finished();
    return;
  }

  auto parameters = TrainingParameters {
      .width = width_,
      .height = height_,
      .iterations = iterations_,
      .radius0 = radius0_,
      .sigma0 = sigma0_,
      .alpha0 = alpha0_,
      .initMode = init_mode_,
      .initMin = min_,
      .initMax = max_
  };

  emit sendRefreshTraining(parameters);
}

/**
 *
 * @param input
 */
void SelfOrganizingMap::trainingInputLoaded(std::shared_ptr<const Input> input) {
  training_input_ = std::move(input);
  analyzing_input_.reset();
  map_.reset();
  umatrix_.reset();
  training_winners_.reset();
  analyzing_winners_.reset();

  computeClasses();
  computeFeatures();

  loaded_ = true;
  emit loadedChanged();
  emit classesChanged();
  emit featuresChanged();
  refreshDisplay("Data loaded", 1, 1, true);
}

/**
 *
 * @param message
 * @param step
 * @param steps
 * @param map
 * @param umatrix
 * @param winners
 */
void SelfOrganizingMap::trainingProgress(
  QString message, int step, int steps,
  std::shared_ptr<const Map> map,
  std::shared_ptr<const UMatrix> umatrix,
  std::shared_ptr<const Winners> winners
) {
  map_ = std::move(map);
  umatrix_ = std::move(umatrix);
  training_winners_ = std::move(winners);

  refreshDisplay(message, step, steps);
  emit sendContinueTraining();
}

/**
 *
 * @param message
 */
void SelfOrganizingMap::trainingFinished(QString message) {
  refreshDisplay(message, iterations_, iterations_, true);
  emit finished();
}

/**
 *
 * @param input
 */
void SelfOrganizingMap::analyzingInputLoaded(std::shared_ptr<const Input> input) {
  analyzing_input_ = std::move(input);
  emit progress("Analysis started", 0, 1);
  emit sendContinuAnalyzing();
}

/**
 *
 * @param winners
 */
void SelfOrganizingMap::analyzingFinished(std::shared_ptr<const Winners> winners) {
  analyzing_winners_ = std::move(winners);
  refreshDisplay(winners ? "Analysis finished" : "Analysis aborted", 0, 1, true);
  emit finished();
}

/**
 *
 * @param url
 */
void SelfOrganizingMap::analyzeFile(const QUrl &url) {
  emit sendStartAnalyzing(url.toLocalFile());
}

/**
 *
 * @param feature
 */
void SelfOrganizingMap::setDisplayFeature(int feature) {
  if(display_feature_ == feature) return;
  display_feature_ = feature;
  emit displayFeatureChanged();
  refreshDisplay();
}
