/*
 * @file         somlib.h
 * @brief        L3 IA et Apprentissage - Déclarations
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-04-09
 */

#ifndef SOMLIB_H
#define SOMLIB_H

#ifdef __cplusplus
extern "C" {
#else
#include <stdbool.h>
#endif

/** Palettes de couleurs **/
typedef enum {
  PALETTE_RAINBOW,
  PALETTE_DARK_BODY,
  PALETTE_GRAY_SCALE,
  PALETTE_BLUE_ORANGE,
  PALETTE_BLUE_RED,
  PALETTE_VIRIDIS,
  PALETTE_INFERNO,
  PALETTE_END,
  PALETTE_INVERT = 0x80
} PALETTE;

/** Modes d'initialisation des poids **/
typedef enum {
  INIT_MODE_ZERO, //< Initialisé avec des zéros
  INIT_MODE_RANDOM, //< Initialisé aléatoirement entre 0 et 1
  INIT_MODE_AVERAGE //< Initialisé selon un vecteur moyen
} INIT_MODE;

/** Mode d'évolution de alpha, du rayon et de sigma **/
typedef enum {
  DECAY_MODE_LINEAR,
  DECAY_MODE_INVERSE,
  DECAY_MODE_EXPONENTIAL
} DECAY_MODE;

/** Fonctions de voisinage **/
typedef enum {
  NEIGHBORHOOD_FUNCTION_BUBBLE,
  NEIGHBORHOOD_FUNCTION_GAUSSIAN,
  NEIGHBORHOOD_FUNCTION_MEXICAN_HAT,
  NEIGHBORHOOD_FUNCTION_EPANECHNIKOV
} NEIGHBORHOOD_FUNCTION;

/** Classes des entrées **/
typedef struct {
  char *name;
  unsigned char r, g, b;
} Class;

/** Indices **/
typedef int SampleIndex;
typedef int ClassIndex;
typedef int NeuronIndex;

/** Entrées **/
typedef struct {
  int max_line_size; //< Longueur maximale d'une ligne du fichier d'entrées.
  int nb_dimensions; //< Nombre de dimensions du fichier d'entrées.
  int nb_samples; //< Nombre d'échantillons (de lignes) du fichier d'entrées.
  double *values; //< Les valeurs du fichier d'entrées.
  Class* classes; //< Les classes (dédupliquées).
  int nb_classes; //< Le nombre de classes.
  ClassIndex *samples_classes;  //< Les classes des valeurs d'entrées (une classe par échantillon).
} Input;

/** Neurones **/
typedef struct {
  double *weights; //< Les poids associés aux neurones
  ClassIndex class_index; //< Classe (index) associée à ce neurone
} Neuron;

/** Carte (matrice) de neurones **/
typedef struct {
  int nb_weights; //< Nombre de poids pour chaque neurone
  int nb_lines; //< Nombre de lignes de la matrice
  int nb_columns; //< Nombre de colonnes de la matrice
  Neuron *neurons; //< Neurones
} Map;

/** Gagnants (GMU) **/
typedef struct {
  NeuronIndex *neurons; //< Les indices des neurones gagnants (BMU)
  int nb_winners; //< Nombre d'éléments
} Winners;

/** Liste de "Best matching unit" - Les neurones les plus proches d'une entrée **/
typedef struct {
  NeuronIndex *indexes; // Indices des BMU
  int nb_bmus; // Nombre de BMU
} BmuArray;

/** Entrainement **/
struct Training;
typedef struct Training Training;
typedef int (*RadiusDecayFunction)(int radius0, int iteration, int iterations);
typedef int (*SigmaDecayFunction)(int sigma0, int iteration, int iterations);
typedef double (*AlphaDecayFunction)(double alpha0, int iteration, int iterations);
typedef double (*NeighborhoodFunction)(double distance, int radius, int sigma);

struct Training {
  int nb_iterations; //< Nombre d'itérations.
  int iteration; //< Itération.
  int radius0; //< Rayon du voisinage initial.
  int sigma0; //< Rayon du voisinage initial.
  double alpha0; //< Coefficient (taux) d'apprentissage initial.
  int nb_samples; //< Nombre d'échantillons.
  RadiusDecayFunction radius_decay; //< Mode d'évolution du rayon/sigma
  SigmaDecayFunction sigma_decay; //< Mode d'évolution du rayon/sigma
  AlphaDecayFunction alpha_decay; //< Mode d'évolution du facteur d'apprentissage (alpha)
  NeighborhoodFunction neighborhood_function; //< Fonction de voisinage
  BmuArray *bmu_list; // Liste de "Best matching units".
  SampleIndex *indexes; // séquence d'indices (faux pointeurs)
};

/** Analyse **/
typedef struct {
  int radius0; //< Rayon du voisinage initial.
  int sigma0; //< Ecart type initial.
  double alpha0; //< Coefficient (taux) d'apprentissage initial.
  int nb_samples; //< Nombre d'échantillons.
  BmuArray *bmu_list; // Liste de "Best matching units".
  SampleIndex *indexes; // séquence d'indices (faux pointeurs)
} Analyzing;

/** U-Matrice **/
typedef struct {
  int nb_lines; //< Nombre de lignes de la matrice
  int nb_columns; //< Nombre de colonnes de la matrice
  int map_nb_lines; //< Nombre de lignes de la carte
  int map_nb_columns; //< Nombre de colonnes de la carte
  double *weights; //< Poids
} UMatrix;

/** Entrées **/

/**
 * Lecture d'un tableau de vecteurs depuis un fichier texte suivant un ordre donné.
 *
 * @param filename Chemin du fichier texte à lire.
 * @param vectors Pointeur sur le premier élément du tableau de vecteurs.
 * @param shuffle_order Ordre de lecture du tableau de vecteurs (numéros de vecteurs).
 * @param nb_vectors Nombre de vecteurs.
 * @param nb_floats Nombre de flottants pour chaque vecteur.
 * @param order Ordre (aléatoire ou séquentiel) de la lecture.
*/
Input *input_read(const char *filename);

/**
 * Normalise les données d'entrée.
 *
 * @param input Un pointeur sur les données d'entrée.
 */
void input_normalize(Input *input);

/**
 * Retourne les données d'un échantillon
 *
 * @param input Les entrées
 * @param sample Le numéro de l'échantillon
 * @return les données de l'échantillon
 */
double *input_data(Input *input, int sample);

/**
 * Retourne les données d'un échantillon
 *
 * @param input Les entrées
 * @param sample Le numéro de l'échantillon
 * @return les données de l'échantillon
 */
const double *input_const_data(const Input *input, int sample);

/**
 * Détruit les données d'entrée.
 *
 * @param input Un pointeur sur les  données d'entrée.
 */
void input_destroy(const Input *input);

/**
 * Test si des données sont compatible avec celles d'entrainement.
 *
 * @param trainedInput Un pointeur sur les données d'entrainement.
 * @param input Un pointeur sur les données d'entrée à tester.
 * @return true si les données sont compatibles, false sinon.
 */
bool input_is_compatible(const Input *trainedInput, const Input *input);

/** Vecteurs **/

/**
 * Initialise un ordre séquentiel.
 *
 * @param order Le tableau d'entiers représentant l'ordre de parcours.
 * @param nb Nombre d'entiers dans le tableau order.
*/
int *vector_create_sequence(int nb);

/**
 * Mélange un tableau d'entiers.
 *
 * @param order Le tableau d'entiers à mélanger.
 * @param nb Nombre d'entiers dans le tableau order.
*/
void vector_shuffle(int *vector, int nb);

/**
 * Normalise un vecteur.
 *
 * @param values Un pointeur sur les valeurs du vecteur.
 * @param nb_values Le nombre de aleurs du vecteur.
 * @return La normal du vecteur.
 */
double vector_normalize(double *values, int nb_values);

/**
 * Calcul la distance euclidienne entre deux vecteurs.
 *
 * @param values1 Les valeurs du premier vecteur.
 * @param values2 Les valeurs du deuxième vecteur.
 * @param nb_values Le nombre de valeurs des deux vecteurs.
 * @return La distance euclidienne entre les deux vecteurs.
 */
double vector_euclidean_distance(const double *first, const double *second, int nb_values);

/**
 * Calcul le vecteur moyen d'un ensemble de vecteurs.
 *
 * @param values Un pointeur sur la première valeur du premier vecteur.
 * @param nb_components Le nombre de composantes de chanque vecteurs.
 * @param nb_vectors Le nombre de vecteur.
 * @return Les valeurs du vecteur moyen.
 */
double *vector_compute_means(const double *values, int nb_components, int nb_vectors);

/** Carte (matrice) de neurones **/

/**
 * Génère une carte de neurones avec poids aléatoires dans un interval.
 *
 * @param nb_lines Nombre de lignes de la carte.
 * @param nb_columns Nombre de colonnes de la carte.
 * @param nb_weights Nombre de poids pour chaque neurone.
 * @param init_mode Mode d'initialisation des poids des neurones.
 * @param means Valeurs moyennes pour les poids, ou NULL pour centrer en entre min et max.
 * @param min Valeur minimum (incluse) pour chaque nombre flottant.
 * @param max Valeur maximum (incluse) pour chaque nombre flottant.
*/
Map *map_generate(
    int nb_lines, int nb_columns, int nb_weights,
    INIT_MODE init_mode, const double *means, double min, double max
);

/**
 * Retourne le nombre total de neurones dans la carte.
 *
 * @param map La carte de neurones.
 * @return Le nombre total de neurones dans la carte.
 */
int map_nb_neurones(const Map *map);

/**
 * Classifie les neurones en fonction des valeurs d'entrée.
 * Le neurone aura l'étiquette de la valeur d'entrée la plus proche.
 *
 * @param map La carte de neurones.
 * @param input Les valeurs d'entrée.
 */
void map_classify(Map *map, const Input *input);

/**
 * Copie une carte de neurones.
 *
 * @param map La carte à copier.
 * @return Une copie de la carte.
 */
Map *map_copy(const Map *map);

/**
 * Destruction d'une carte.
 *
 * @param map La carte à détruire.
*/
void map_destroy(const Map *map);

/** Gagnants (BMU) **/

/**
 * Initialise les gagnants.
 *
 * @param nb_samples Le nombre d'échantillons (i.e. de gagnants).
 * @return Un pointeur sur les gagnants.
 */
Winners *winners_init(int nb_samples);

/**
 * Détruits des gagnants.
 *
 * @param winners Un pointeur sur les gagnants.
 */
void winners_destroy(const Winners *winners);

/** Entrainement **/

/**
 * Initialise l'entrainement d'une carte de neurones avec des données d'entrée.
 *
 * @param nb_samples Le nombre de données d'entrée.
 * @param nb_neurons Le nombre de neurones.
 * @param nb_iterations Le nombre d'itérations.
 * @param radius0 Le rayon de départ des neurones à activer.
 * @param sigma0 Le l'écart type de départ.
 * @param alpha0 Le facteur d'apprentissage de départ.
 * @param radius_decay_mode Mode d'évolution du rayon.
 * @param sigma_decay_mode Mode d'évolution de sigma.
 * @param alpha_decay_mode Mode d'évolution du facteur d'apprentissage (alpha).
 * @param neighborhood_function Fonction de voisinage.
 * @return Un pointeur sur les données d'entrainement.
 */
Training *training_init(
    int nb_samples, int nb_neurons, int nb_iteration,
    int radius0, int sigma0, double alpha0,
    DECAY_MODE radius_decay_mode,
    DECAY_MODE sigma_decay_mode,
    DECAY_MODE alpha_decay_mode,
    NEIGHBORHOOD_FUNCTION neighborhood_function
);

/**
 * Effectue l'étape suivante de l'entrainement.
 *
 * @param map La carte des neurones.
 * @param training Un pointeur sur les données d'entrainement.
 * @param winners Un pointeur des les gagnants de cette étape d'entrainement.
 * @param input Les données d'entrainement.
 * @return true si l'étape suivante de l'entrainement a été effectuée, false s'il n'y a pas de prochaine étape.
 */
bool training_next(Map *map, Training *training, Winners *winners, const Input *input);

/**
 * Détruit les données d'entrainement.
 *
 * @param training Les données d'entrainement.
 */
void training_destroy(const Training *training);

/** Analyse **/

/**
 * Initialisation d'une analyse.
 *
 * @param nb_samples Le nombre de données d'entrée.
 * @param radius0 Le rayon de départ des neurones à activer.
 * @param sigma0 L'écart type des neurones à activer.
 * @param alpha0 Le facteur d'apprentissage de départ.
 */
Analyzing *analyzing_init(int nb_samples, int nb_neurons, int radius0, int sigma0, double alpha0);

/**
 * Analyse.
 *
 * @param map Carte de neurones.
 * @param analyzing Données internes pour l'analyse.
 * @param winners Gagnants (BMU).
 * @param input Données d'entrée.
 */
void analyzing(const Map *map, Analyzing *analyzing, Winners *winners, const Input *input);

/**
 * Destruction de l'analyse.
 *
 * @param analyzing Données internes pour l'analyse.
 */
void analyzing_destroy(const Analyzing *analyzing);

/** Entrées / Sorties **/

/**
 * Chargement en mémoire des données.
 *
 * @param input Les données d'entrée.
 * @param map La carte de neurones.
 * @param winners Pointeur sur les gagnants.
 * @param filename Le nom du fichier,
 * @return true si le chargement c'est bien passé, false sinon.
 */
bool data_load(Input *input, Map *map, Winners *winners, const char *filename);

/**
 * Sauvegarde des données.
 *
 * @param input Les données d'entrée.
 * @param map La carte de neurones.
 * @param winners Pointeur sur les gagnants.
 * @param filename Le nom du fichier,
 * @return true si la sauvegarde s'est bien passée, false sinon.
 */
bool data_save(const Input *input, const Map *map, const Winners *winners, const char *filename);

/** U-Matrix **/

/**
 * Alloue la mémoire et genère une U-Matrice à partir d'une carte de neurones.
 *
 * @param map La carte de neurones.
 * @return Un pointeur sur la U-Matrice.
 */
UMatrix *umatrix_generate(const Map *map);

/**
 * Normalise une U-Matrice.
 *
 * @param umatrix La U-Matrice.
 */
void umatrix_normalize(UMatrix *umatrix);

/**
 * Détruit une U-Matrice.
 *
 * @param umatrix La U-Matrice.
 */
void umatrix_destroy(const UMatrix *umatrix);

/** Palettes de couleurs **/

/**
 * Conversion d'une valeur en couleur avec une palette de couleurs.
 *
 * @param value La valeur à convertir.
 * @param palette La palette de couleurs à utiliser.
 * @param red La composante rouge de la couleur.
 * @param green La composante verte de la couleur.
 * @param blue La composante bleue de la couleur.
 */
void colormap_convert(double value, PALETTE palette, unsigned char* red, unsigned char* green, unsigned char* blue);

/** Utilitaires **/


/**
 * Allocation mémoire. Affiche un message d'erreur en cas d'échec.
 *
 * @param size La taille de la mémoire à allouer.
 * @return Un pointeur sur la mémoire allouée.
 */
void *checked_malloc(size_t size);

/**
 * Affiche un message et la dernière erreur.
 *
 * @param format Le format du message à afficher.
 * @param ... Les arguments pour le format (cf. printf)
 */
void print_last_error(const char *format, ...);

/**
 * Affiche un message d'erreur.
 *
 * @param format Le format du message à afficher.
 * @param ... Les arguments pour le format (cf. printf)
 */
void print_error(const char *format, ...);

#ifdef __cplusplus
}
#endif

#endif // SOMLIB_H
