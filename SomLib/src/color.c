/*
 * @file         color.c
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-01-08
 */

#include "private.h"

/**
 * Couleur (RGB).
 */
typedef struct {
  double r, g, b;
} RGB;

/**
 * Données pour interpoler la palette Viridis.
 */
static const RGB viridis[] = {
  {0.267004, 0.004874, 0.329415},  // bleu foncé
  {0.229739, 0.322361, 0.545706},  // bleu
  {0.127568, 0.566949, 0.550556},  // cyan
  {0.369214, 0.788888, 0.382914},  // vert
  {0.993248, 0.906157, 0.143936}   // jaune
};

/**
 * Données pour interpoler la palette Inferno.
 */
static const RGB inferno[] = {
  {0.001462, 0.000466, 0.013866},  // Noir
  {0.267004, 0.004874, 0.329415},  // Violet foncé
  {0.717647, 0.071713, 0.040665},  // rouge
  {0.976471, 0.983392, 0.053911}   // jaune clair
};

typedef void (*ConvertFn)(double value, unsigned char* red, unsigned char* green, unsigned char* blue);

/**
 * Conversion d'un réel en entier non signé sur un octet.
 *
 * @param x La valeur à convertir.
 * @return Un octet.
 */
static inline unsigned char clamp(double x) {
  return x < 0 ? 0 : (unsigned char)(x > 255 ? 255 : x);
}

/**
 * Conversion d'une couleur en HSL (teinte, saturation, lumière) en RGB (rouge, vert, bleu)
 *
 * @param hue Teinte
 * @param saturation Saturation
 * @param lightness lumière
 * @param r Rouge
 * @param g Vert
 * @param b Bleu
 */
static void hsl_to_rgb(double hue, double saturation, double lightness, unsigned char* r, unsigned char* g, unsigned char* b) {
  double c = (1 - fabs(2 * lightness - 1)) * saturation;
  double x = c * (1 - fabs(fmod(hue / 60.0, 2) - 1));
  double m = lightness - c / 2;
  double rPrime, gPrime, bPrime;

  if(hue >= 0 && hue < 60) {
    rPrime = c;
    gPrime = x;
    bPrime = 0;
  } else if(hue >= 60 && hue < 120) {
    rPrime = x;
    gPrime = c;
    bPrime = 0;
  } else if(hue >= 120 && hue < 180) {
    rPrime = 0;
    gPrime = c;
    bPrime = x;
  } else if(hue >= 180 && hue < 240) {
    rPrime = 0;
    gPrime = x;
    bPrime = c;
  } else if(hue >= 240 && hue < 300) {
    rPrime = x;
    gPrime = 0;
    bPrime = c;
  } else {
    rPrime = c;
    gPrime = 0;
    bPrime = x;
  }

  *r = (unsigned char)((rPrime + m) * 255);
  *g = (unsigned char)((gPrime + m) * 255);
  *b = (unsigned char)((bPrime + m) * 255);
}

/**
 * Conversion d'une valeur en couleur avec la palette de couleurs du corps noir.
 *
 * @param value La valeur à convertir.
 * @param red La composante rouge de la couleur.
 * @param green La composante verte de la couleur.
 * @param blue La composante bleue de la couleur.
 */
static void colormap_black_body_color(double value, unsigned char* red, unsigned char* green, unsigned char* blue) {
  double hue = 60 * value; // Teinte de 0 (rouge) à 60 (jaune)
  double saturation = 1.0;
  double lightness = 0.1 + 0.8 * value; // Luminosité de 0.1 (sombre) à 0.9 (clair)
  hsl_to_rgb(hue, saturation, lightness, red, green, blue);
}

/**
 * Conversion d'une valeur en couleur avec la palette de couleurs arc-en-ciel.
 *
 * @param value La valeur à convertir.
 * @param red La composante rouge de la couleur.
 * @param green La composante verte de la couleur.
 * @param blue La composante bleue de la couleur.
 */
static void colormap_rainbow(double value, unsigned char* red, unsigned char* green, unsigned char* blue) {
  double hue = value * 360;
  hsl_to_rgb(hue, 1.0, 0.5, red, green, blue);
}

/**
 * Conversion d'une valeur en couleur avec la palette de couleurs grises.
 *
 * @param value La valeur à convertir.
 * @param red La composante rouge de la couleur.
 * @param green La composante verte de la couleur.
 * @param blue La composante bleue de la couleur.
 */
static void colormap_gray_scale(double value, unsigned char* red, unsigned char* green, unsigned char* blue) {
  *red = *green = *blue = clamp(value * 255);
}

/**
 * Conversion d'une valeur en couleur avec la palette de couleurs bleu-orange.
 *
 * @param value La valeur à convertir.
 * @param red La composante rouge de la couleur.
 * @param green La composante verte de la couleur.
 * @param blue La composante bleue de la couleur.
 */
static void colormap_blue_orange(double value, unsigned char* red, unsigned char* green, unsigned char* blue) {
  double hue = 240 - value * 201; // Du bleu (H = 240) au orange (H = 39)
  hsl_to_rgb(hue, 1.0, 0.5, red, green, blue);
}

/**
 * Conversion d'une valeur en couleur avec la palette de couleurs bleu-rouge.
 *
 * @param value La valeur à convertir.
 * @param red La composante rouge de la couleur.
 * @param green La composante verte de la couleur.
 * @param blue La composante bleue de la couleur.
 */
static void colormap_blue_red(double value, unsigned char* red, unsigned char* green, unsigned char* blue) {
  double hue = 240 + value * 120; // Du bleu (H = 240) au violet (H = 360), puis de 0 à 60 (rouge)
  if(hue > 360) hue -= 360;
  hsl_to_rgb(hue, 1.0, 0.5, red, green, blue);
}

/**
 * Interpolation entre deux couleurs.
 *
 * @param col1 Première couleur.
 * @param col2 Deuxième couleur.
 * @param fraction Fraction entre les deux couleurs.
 * @return La couleur interpolée.
 */
static RGB interpolate(RGB col1, RGB col2, double fraction) {
  RGB color;
  color.r = col1.r + fraction * (col2.r - col1.r);
  color.g = col1.g + fraction * (col2.g - col1.g);
  color.b = col1.b + fraction * (col2.b - col1.b);
  return color;
}

/**
 * Conversion d'une valeur en couleur par interpolation.
 *
 * @param value La valeur à convertir.
 * @param colors Les points dans l'espace des couleurs pour l'interpolation.
 * @param nb_colors Le nombre de points dans l'espace des couleurs pour l'interpolation.
 * @return La couleur interpolée.
 */
static RGB get_interpolated_color(double value, const RGB* colors, size_t nb_colors) {
  if(value <= 0.0)
    return colors[0];
  if(value >= 1.0)
    return colors[nb_colors - 1];

  value = value * (double)(nb_colors - 1);
  int idx1 = (int)value;
  int idx2 = idx1 + 1;
  double fraction = value - idx1;
  return interpolate(colors[idx1], colors[idx2], fraction);
}

/**
 * Conversion d'une valeur en couleur avec la palette de couleurs Viridis.
 *
 * @param value La valeur à convertir.
 * @param red La composante rouge de la couleur.
 * @param green La composante verte de la couleur.
 * @param blue La composante bleue de la couleur.
 */
static void colarmap_viridis(double value, unsigned char* red, unsigned char* green, unsigned char* blue) {
  RGB rgb = get_interpolated_color(value, viridis, sizeof viridis / sizeof viridis[0]);
  *red = (unsigned char)(rgb.r * 255);
  *green = (unsigned char)(rgb.g * 255);
  *blue = (unsigned char)(rgb.b * 255);
}

/**
 * Conversion d'une valeur en couleur avec la palette de couleurs Inferno.
 *
 * @param value La valeur à convertir.
 * @param red La composante rouge de la couleur.
 * @param green La composante verte de la couleur.
 * @param blue La composante bleue de la couleur.
 */
static void colormap_inferno(double value, unsigned char* red, unsigned char* green, unsigned char* blue) {
  RGB rgb = get_interpolated_color(value, inferno, sizeof inferno / sizeof inferno[0]);
  *red = (unsigned char)(rgb.r * 255);
  *green = (unsigned char)(rgb.g * 255);
  *blue = (unsigned char)(rgb.b * 255);
}

/**
 * Conversion d'une valeur en couleur avec une palette de couleurs.
 *
 * @param value La valeur à convertir.
 * @param palette La palette de couleurs à utiliser.
 * @param red La composante rouge de la couleur.
 * @param green La composante verte de la couleur.
 * @param blue La composante bleue de la couleur.
 */
void colormap_convert(double value, PALETTE palette, unsigned char* red, unsigned char* green, unsigned char* blue) {
  static const ConvertFn convert[PALETTE_END] = {
      colormap_rainbow,
      colormap_black_body_color,
      colormap_gray_scale,
      colormap_blue_orange,
      colormap_blue_red,
      colarmap_viridis,
      colormap_inferno
  };

  if(palette & PALETTE_INVERT) value = 1.0 - value;
  palette = palette & ~PALETTE_INVERT;
  if(palette >= PALETTE_END) palette = PALETTE_RAINBOW;
  convert[palette](value, red, green, blue);
}
