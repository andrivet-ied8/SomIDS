/*
 * @file         io.c
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-04-09
 */

#include "private.h"

static char MAGIC[] = "SOM1";

/** Structure du fichier:
 *
 *  Header: Entête avec toutes les valeurs fixes
 *  Classes: Les classes
 *      nom: taille du nom, characters, 0
 *      couleur: R, G, B
 *  Input: Les valeurs d'entrée (entrainement)
 *      Values: Les valeurs d'entrée
 *      Classes: Les index des classes des valeurs d'entrées
 *  Neurons
 *      Neuron weights: Les poids du neurone
 *      Neuron class: Classe (index) associée à ce neurone
 *  Winners: Les indices des neurones gagnants
 */

typedef struct {
  char magic[4];
  int nb_dimensions; //< Nombre de dimensions (poids)
  int nb_lines; //< Nombre de lignes de la matrice
  int nb_columns; //< Nombre de colonnes de la matrice
  int nb_samples; // Nombre de valeurs d'entrée (entrainement)
  int max_line_size;
  int nb_classes; //< Le nombre de classes (uniques).
  int nb_winners; //< Nombre d'éléments dans winners
} Header;


/**
 * Sauve l'entète du fichier.
 *
 * @param file Un pointeur représentant le fichier.
 * @param input Les données d'entrée.
 * @param map La carte de neurones.
 * @param winners Les gagnants (BMU)
 * @param filename Le nom du fichier,
 * @return true si la sauvegarde s'est bien passée, false sinon.
 */
static bool save_header(FILE *file, const Input *input, const Map *map, const Winners *winners, const char *filename) {
  Header header;
  memcpy(header.magic, MAGIC, sizeof header.magic);
  header.nb_dimensions = map->nb_weights;
  header.nb_lines = map->nb_lines;
  header.nb_columns = map->nb_columns;
  header.nb_samples = input->nb_samples;
  header.max_line_size = input->max_line_size;
  header.nb_classes = input->nb_classes;
  header.nb_winners = winners->nb_winners;

  if(fwrite(&header, sizeof(Header), 1, file) != 1) {
    print_last_error("Erreur lors de l'écriture de l'entête du fichier '%s'", filename);
    return false;
  }

  return true;
}

/**
 * Charge en mémoire l'entête du fichier.
 *
 * @param file Un pointeur représentant le fichier.
 * @param headerl L'entête du fichier.
 * @param filename Le nom du fichier,
 * @return true si le chargement c'est bien passé, false sinon.
 */
static bool load_header(FILE *file, Header *header, const char *filename) {
  if(fread(header, sizeof(Header), 1, file) != 1) {
    print_last_error("Erreur lors de la lecture du fichier '%s'", filename);
    return false;
  }

  if(memcmp(header->magic, MAGIC, sizeof header->magic) != 0) {
    print_last_error("Erreur lors de la lecture du fichier '%s', le fichier est corrompu", filename);
    return false;
  }

  return true;
}

/**
 * Sauvegarde des classes des données d'entrée.
 *
 * @param file Un pointeur représentant le fichier.
 * @param input Les données d'entrée.
 * @param filename Le nom du fichier,
 * @return true si la sauvegarde s'est bien passée, false sinon.
 */
static bool save_classes(FILE *file, const Input *input, const char *filename) {
  unsigned char rgb[3];
  const Class *cls = input->classes;
  for(int i = 0; i < input->nb_classes; ++i, ++cls) {
    size_t length = strlen(cls->name);
    if(fwrite(&length, sizeof(size_t), 1, file) != 1) {
      print_last_error("Erreur lors de l'écriture de la taille de la classe '%s' dans le fichier '%s'", cls->name, filename);
      return false;
    }
    if(fwrite(cls->name, length + 1, 1, file) != 1) {
      print_last_error("Erreur lors de l'écriture de la classe '%s' dans le fichier '%s'", cls->name, filename);
      return false;
    }

    rgb[0] = cls->r;
    rgb[1] = cls->g;
    rgb[2] = cls->b;
    if(fwrite(rgb, sizeof(unsigned char), 3, file) != 3) {
      print_last_error("Erreur lors de l'écriture de la classe '%s' dans le fichier '%s'", cls->name, filename);
      return false;
    }
  }

  return true;
}

/**
 * Chargement des classes des données d'entrée.
 *
 * @param file Un pointeur représentant le fichier.
 * @param input Les données d'entrée.
 * @param filename Le nom du fichier,
 * @return true si le chargement c'est bien passé, false sinon.
 */
static bool load_classes(FILE *file, Input *input, const char *filename) {
  unsigned char rgb[3];
  size_t length = 0;
  Class *cls = input->classes;
  for(int i = 0; i < input->nb_classes; ++i, ++cls) {
    if(fread(&length, sizeof(size_t), 1, file) != 1) {
      print_last_error("Erreur lors de la lecture de la taille d'une classe dans le fichier '%s'", filename);
      return false;
    }

    cls->name = malloc(length + 1);
    if(fread(cls->name, length + 1, 1, file) != 1) {
      print_last_error("Erreur lors de la lecture d'une classe dans le fichier '%s'", cls->name, filename);
      return false;
    }

    if(fread(rgb, sizeof(unsigned char), 3, file) != 3) {
      print_last_error("Erreur lors de la lecture de la classe '%s' dans le fichier '%s'", cls->name, filename);
      return false;
    }

    cls->r = rgb[0];
    cls->g = rgb[1];
    cls->b = rgb[2];
  }

  return true;
}

/**
 * Sauvegarde des données d'entrée (squf les classes).
 *
 * @param file Un pointeur représentant le fichier.
 * @param input Les données d'entrée.
 * @param filename Le nom du fichier,
 * @return true si la sauvegarde s'est bien passée, false sinon.
 */
static bool save_input(FILE *file, const Input *input, const char *filename) {
  if(fwrite(input->values, sizeof(double), input->nb_samples, file) != (size_t)(input->nb_samples)) {
    print_last_error("Erreur lors de l'écriture des valeurs d'entrée dans le fichier '%s'", filename);
    return false;
  }
  if(fwrite(input->samples_classes, sizeof(ClassIndex), input->nb_samples, file) != (size_t)input->nb_samples) {
    print_last_error("Erreur lors de l'écriture des classes des valeurs d'entrée dans le fichier '%s'", filename);
    return false;
  }
  return true;
}

/**
 * Chargement des données d'entrée (squf les classes).
 *
 * @param file Un pointeur représentant le fichier.
 * @param input Les données d'entrée.
 * @param filename Le nom du fichier,
 * @return true si le chargement c'est bien passé, false sinon.
 */
static bool load_input(FILE *file, Input *input, const char *filename) {
  if(fread(input->values, sizeof(double), input->nb_samples, file) != (size_t)input->nb_samples) {
    print_last_error("Erreur lors de la lecture des valeurs d'entrée dans le fichier '%s'", filename);
    return false;
  }
  if(fread(input->samples_classes, sizeof(ClassIndex), input->nb_samples, file) != (size_t)input->nb_samples) {
    print_last_error("Erreur lors de la lecture des classes des valeurs d'entrée dans le fichier '%s'", filename);
    return false;
  }
  return true;
}

/**
 * Sauvegarde des neurones.
 *
 * @param file Un pointeur représentant le fichier.
 * @param map La carte de neurones.
 * @param filename Le nom du fichier,
 * @return true si la sauvegarde s'est bien passée, false sinon.
 */
static bool save_neurons(FILE *file, const Map *map, const char *filename) {
  int nb_neurons = map->nb_lines * map->nb_columns;
  const Neuron *neuron = map->neurons;
  for(int i = 0; i < nb_neurons; ++i, ++neuron) {
    if(fwrite(neuron->weights, sizeof(double), map->nb_weights, file) != (size_t)map->nb_weights) {
      print_last_error("Erreur lors de l'écriture des poids du neurone %i dans le fichier '%s'", i, filename);
      return false;
    }
    if(fwrite(&neuron->class_index, sizeof(int), 1, file) != 1) {
      print_last_error("Erreur lors de l'écriture de la classe du neurone %i dans le fichier '%s'", i, filename);
      return false;
    }
  }
  return true;
}

/**
 * Chargement des neurones.
 *
 * @param file Un pointeur représentant le fichier.
 * @param map La carte de neurones.
 * @param filename Le nom du fichier,
 * @return true si le chargement c'est bien passé, false sinon.
 */
static bool load_neurons(FILE *file, Map *map, const char *filename) {
  int nb_neurons = map->nb_lines * map->nb_columns;
  Neuron *neuron = map->neurons;
  for(int i = 0; i < nb_neurons; ++i, ++neuron) {
    neuron->weights = malloc(sizeof(double) * map->nb_weights);
    if(!neuron->weights) return false;
    if(fread(neuron->weights, sizeof(double), map->nb_weights, file) != (size_t)map->nb_weights) {
      print_last_error("Erreur lors de la lecture des poids du neurone %i dans le fichier '%s'", i, filename);
      return false;
    }
    if(fread(&neuron->class_index, sizeof(int), 1, file) != 1) {
      print_last_error("Erreur lors de la lecture de la classe du neurone %i dans le fichier '%s'", i, filename);
      return false;
    }
  }
  return true;
}

/**
 * Sauvegarde des gagnants (BMU des données d'entrée).
 *
 * @param file Un pointeur représentant le fichier.
 * @param winners Pointeur sur les gagnants.
 * @param filename Le nom du fichier,
 * @return true si la sauvegarde s'est bien passée, false sinon.
 */
static bool save_winners(FILE *file, const Winners *winners, const char *filename) {
  if(fwrite(winners->neurons, sizeof(NeuronIndex), winners->nb_winners, file) != (size_t)winners->nb_winners) {
    print_last_error("Erreur lors de l'écriture des gagnants dans le fichier '%s'", filename);
    return false;
  }
  return true;
}

/**
 * Chargement des gagnants (BMU des données d'entrée).
 *
 * @param file Un pointeur représentant le fichier.
 * @param winners Pointeur sur les gagnants.
 * @param filename Le nom du fichier,
 * @return true si le chargement c'est bien passé, false sinon.
 */
static bool load_winners(FILE *file, Winners *winners, const char *filename) {
  if(fread(winners->neurons, sizeof(NeuronIndex), winners->nb_winners, file) != (size_t)winners->nb_winners) {
    print_last_error("Erreur lors de la lecture des gagnants dans le fichier '%s'", filename);
    return false;
  }
  return true;
}

/**
 * Sauvegarde des données.
 *
 * @param input Les données d'entrée.
 * @param map La carte de neurones.
 * @param winners Pointeur sur les gagnants.
 * @param filename Le nom du fichier,
 * @return true si la sauvegarde s'est bien passée, false sinon.
 */
bool data_save(const Input *input, const Map *map, const Winners *winners, const char *filename) {
  FILE *file = fopen(filename, "w");
  if(!file) {
    print_last_error("Erreur lors de l'ouverture du fichier '%s'", filename);
    return false;
  }

  bool success =
    save_header(file, input, map, winners, filename) &&
    save_classes(file, input, filename) &&
    save_input(file, input, filename) &&
    save_neurons(file, map, filename) &&
    save_winners(file, winners, filename);

  fclose(file);
  return success;
}

/**
 * Chargement en mémoire des données.
 *
 * @param input Les données d'entrée.
 * @param map La carte de neurones.
 * @param winners Pointeur sur les gagnants.
 * @param filename Le nom du fichier,
 * @return true si le chargement c'est bien passé, false sinon.
 */
bool data_load(Input *input, Map *map, Winners *winners, const char *filename) {
  FILE *file = fopen(filename, "r");
  if(!file) {
    print_last_error("Erreur lors de l'ouverture du fichier '%s'", filename);
    return false;
  }

  Header header;
  if(!load_header(file, &header, filename)) {
    fclose(file);
    return false;
  }

  input->max_line_size = header.max_line_size;
  input->nb_dimensions = header.nb_dimensions;
  input->nb_samples = header.nb_samples;
  input->nb_classes = header.nb_classes;
  input->values = malloc(sizeof(double) * input->nb_samples);
  input->classes = malloc(sizeof(Class) * input->nb_classes);
  input->samples_classes = malloc(sizeof(ClassIndex) * input->nb_samples);

  map->nb_weights = header.nb_dimensions;
  map->nb_lines = header.nb_lines;
  map->nb_columns = header.nb_columns;
  map->neurons = malloc(sizeof(Neuron) * map->nb_lines * map->nb_columns);

  winners->nb_winners = header.nb_winners;
  winners->neurons = malloc(sizeof(NeuronIndex) * winners->nb_winners);

  bool success =
      input->values &&
      input->samples_classes &&
      map->neurons &&
      winners->neurons &&
      load_classes(file, input, filename) &&
      load_input(file, input, filename) &&
      load_neurons(file, map, filename) &&
      load_winners(file, winners, filename);

  fclose(file);
  return success;
}
