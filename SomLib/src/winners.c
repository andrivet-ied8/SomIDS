/*
 * @file         winners.c
 * @brief        L3 IA et Apprentissage -
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-04-09
 */

#include "private.h"

/**
 * Initialise les gagnants.
 *
 * @param nb_samples Le nombre d'échantillons (i.e. de gagnants).
 * @return Un pointeur sur les gagnants.
 */
Winners *winners_init(int nb_samples) {
  Winners *winners = checked_malloc(sizeof(Winners));
  if(!winners) return NULL;
  winners->nb_winners = nb_samples;
  winners->neurons = checked_malloc(sizeof(NeuronIndex) * winners->nb_winners);
  if(!winners->neurons) {
    free(winners);
    return NULL;
  }
  return winners;
}

/**
 * Détruits des gagnants.
 *
 * @param winners Un pointeur sur les gagnants.
 */
void winners_destroy(const Winners *winners) {
  if(!winners) return;

  free(winners->neurons);
  free((void*)winners);
}
