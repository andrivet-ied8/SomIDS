/*
 * @file         errors.c
 * @brief        L3 IA et Apprentissage - 
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-01-08
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "private.h"

/**
 * Affiche un message et la dernière erreur.
 *
 * @param format Le format du message à afficher.
 * @param ... Les arguments pour le format (cf. printf)
 */
void print_last_error(const char *format, ...) {
  va_list args1;
  va_start(args1, format);
  va_list args2;
  va_copy(args2, args1);

  char buffer[1 + vsnprintf(NULL, 0, format, args1)];
  va_end(args1);
  vsnprintf(buffer, sizeof buffer, format, args2);
  va_end(args2);

  perror(buffer);
}

/**
 * Affiche un message d'erreur.
 *
 * @param format Le format du message à afficher.
 * @param ... Les arguments pour le format (cf. printf)
 */
void print_error(const char *format, ...) {
  va_list args;
  va_start(args, format);

  vfprintf(stderr, format, args);
  va_end(args);
}

