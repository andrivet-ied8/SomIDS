/*
 * @file         hashmap_str.c
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-01-08
 */

#include "private.h"
#include "hashmap.h"

#define INITIAL_BUCKETS_COUNT 128

/**
 * Noeud pour la liste chainée
 */
typedef struct Node {
  struct Node* next; //< Noeud suivant ou NULL
  const char *key; //< Clef associée à ce noeud
  int value; //< Valeur associée à la clef
}
Node;

/**
 * Table de hashage
 */
typedef struct Hashmap {
  size_t count_elements; //< Nombre total d'élément dans la table de hashage
  Node** buckets; //< Tableau de listes de noeuds (seaux)
  size_t buckets_count; //< Nombre de seaux
  size_t buckets_threshold; //< Seille pour redimensionner la table de hashage (nombre de seaux)
} Hashmap;

/**
 * Itérateur de table de hashage
 */
typedef struct HashmapIterator {
  const Hashmap *hashmap; //< Pointeur sur la table de hashage
  size_t bucket; //< Index du seau courant
  Node *node; //< Noeud courant
} HashmapIterator;

/**
 * Calcul une valeur de hashage à partir d'une chaine de caractères.
 *
 * @param str La chaine de caractères.
 * @return La valeur de hashage.
 */
static unsigned long hash(const char *str) {
  // algorithme djb2
  // http://www.cse.yorku.ca/~oz/hash.html

  unsigned long hash = 5381;

  int c = (unsigned char)(*str);
  while(c) {
    hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    c = (unsigned char)(*str++);
  }

  return hash;
}

/**
 * Compare deux clefs.
 *
 * @param key0 La première clef.
 * @param key1 La deuxième clef.
 * @return true si les clefs sont identique, false sinon.
 */
static bool compare(const char *key0, const char *key1) {
  return 0 == strcmp(key0, key1);
}

/**
 * Calcul l'index de seau à partir du nombre de seaux et d`une clef.
 *
 * @param nb_elements Le nombre de seaux.
 * @param key La clef.
 * @return L'index du seau correspondant à cette clef.
 */
static unsigned long hashmap_get_index(size_t nb_elements, const char *key) {
  return hash(key) % nb_elements;
}

/**
 * Crée une table de hashage.
 *
 * @return Un pointeur sur la table de hashage.
 */
Hashmap *hashmap_new(void) {
  Hashmap *map = (Hashmap*)checked_malloc(sizeof(Hashmap));
  if(!map) return NULL;
  memset(map, 0, sizeof(Hashmap));

  size_t size = INITIAL_BUCKETS_COUNT * sizeof(Hashmap*);

  map->count_elements = 0;
  map->buckets = (Node**)checked_malloc(size);
  if(!map->buckets) {
    free(map);
    return NULL;
  }
  memset(map->buckets, 0, size);
  map->buckets_count = INITIAL_BUCKETS_COUNT;
  map->buckets_threshold = map->buckets_count / 2;

  return map;
}

/**
 * Détruit une table de hashage.
 *
 * @param map La table de hashage.
 */
void hashmap_destroy(Hashmap *map) {
  if(!map) return;

  for(size_t index = 0; index < map->buckets_count; ++index) {
    Node *node = map->buckets[index];
    while(node) {
      free((void*)node->key);
      Node *next = node->next;
      free(node);
      node = next;
    }
  }
  free(map->buckets);
  free(map);
}

/**
 * Indique le nombre d'élément d'une table de hashage.
 *
 * @param map La table de hashage.
 * @return Le nombre d'éléments de cette table.
 */
size_t hashmap_count(const Hashmap *map) {
  return map->count_elements;
}

/**
 * Retourne la valeur associée à une clef dans une table de hashage.
 *
 * @param map La table de hashage.
 * @param key La clef
 * @return La valeur associée à la clef.
 */
int hashmap_get(const Hashmap *map, const char *key) {
  unsigned long index = hashmap_get_index(map->buckets_count, key);
  Node *node = map->buckets[index];

  while(node) {
    if(compare(node->key, key))
      return node->value;
    node = node->next;
  }

  return 0;
}

/**
 * Indique si une table de hashage contient un élément associé à une clef.
 *
 * @param map La table de hashage.
 * @param key La clef
 * @return true si la table de hashage contient un élément associé à la clef.
 */
bool hashmap_contains(const Hashmap *map, const char *key) {
  unsigned long index = hashmap_get_index(map->buckets_count, key);
  Node *node = map->buckets[index];

  while(node) {
    if(compare(node->key, key))
      return true;
    node = node->next;
  }

  return false;
}

/**
 * Retourne le dernier noeud d'une liste liée.
 *
 * @param head La tête de la liste.
 * @return Le dernier noeud de la liste liée ou NULL si la liste est vide.
 */
static Node* hashmap_get_end(Node *head) {
  if(!head) return NULL;
  while(head->next)
    head = head->next;

  return head;
}

/**
 * Redimensionne une table de hashage en augmentant le nombre de seaux.
 *
 * @param map La table de hashage.
 * @return true si la table de hashage a pu être augmentée, false sinon.
 */
static bool hashmap_resize(Hashmap *map) {
  size_t new_buckets_count = map->buckets_count * 2;
  size_t size = new_buckets_count * sizeof(Node*);
  Node **new_buckets = (Node**)checked_malloc(size);
  if(!new_buckets) return false;
  memset(new_buckets, 0, size);

  for(size_t index = 0; index < map->buckets_count; ++index) {
    Node *node = map->buckets[index];
    while(node) {
      unsigned long new_index = hashmap_get_index(new_buckets_count, node->key);
      Node *end = hashmap_get_end(new_buckets[new_index]);
      if(end)
        end->next = node;
      else
        new_buckets[new_index] = node;
      Node *next = node->next;
      node->next = NULL;
      node = next;
    }
  }

  free(map->buckets);
  map->buckets = new_buckets;
  map->buckets_count = new_buckets_count;
  map->buckets_threshold = map->buckets_count * 2;
  return true;
}

/**
 * Associe une valeur à une clef dans une table de hashage.
 *
 * @param map La table de hashage.
 * @param key La clef.
 * @param value La valeur.
 * @return L'ancienne valeur s'il y en avait une, 0 sinon.
 */
int hashmap_set(Hashmap *map, const char *key, int value) {
  unsigned long index = hashmap_get_index(map->buckets_count, key);
  Node *previous = NULL;
  Node *node = map->buckets[index];

  // On cherche la où mettre la valeur
  while(node) {
    // Est-ce la même clef ?
    if(compare(node->key, key)) {
      // Oui, on remplace la valeur et on retourne l'ancienne
      int previous_value = node->value;
      node->value = value;
      return previous_value;
    }
    previous = node;
    node = node->next;
  }

  node = (Node*)checked_malloc(sizeof(Node));
  if(!node) return 0;
  memset(node, 0, sizeof(Node));
  node->next = NULL;
  node->key = strdup(key);
  node->value = value;

  if(previous)
    previous->next = node;
  else
    map->buckets[index] = node;

  map->count_elements++;
  if(map->count_elements >= map->buckets_threshold)
    hashmap_resize(map);

  return 0;
}

/**
 * Enlève une valeur d'une table de hashage.
 *
 * @param map La table de hashage.
 * @param key La clef.
 * @return L'ancienne valeur s'il y en avait une, 0 sinon.
 */
int hashmap_remove(Hashmap *map, const char *key) {
  unsigned long index = hashmap_get_index(map->buckets_count, key);
  Node *previous = NULL;
  Node *node = map->buckets[index];

  while(node) {
    if(compare(node->key, key)) {
      int value = node->value;
      if(previous)
        previous->next = node->next;
      else
        map->buckets[index] = node->next;
      free((void*)node->key);
      free(node);
      map->count_elements--;
      return value;
    }

    previous = node;
    node = node->next;
  }

  return 0;
}

/**
 * Initialise un itérateur de table de hashage.
 *
 * @param hashmap La table de hashage.
 * @return Un pointeur sur l'itérateur.
 */
HashmapIterator *hashmap_init_enum(const Hashmap *hashmap) {
  HashmapIterator *it = checked_malloc(sizeof(HashmapIterator));
  if(!it) return NULL;
  it->hashmap = hashmap;
  it->node = NULL;
  it->bucket = 0;
  return it;
}

/**
 * Retourne la valeur et la clef courant et déplace l'itérateur au prochain élément.
 *
 * @param iterator L'itérateur de table de hashage.
 * @param value La valeur courante.
 * @return La clef courante ou NULL si l'itérateur est déjà à la fin de la table de hashage.
 */
const char *hashmap_next(HashmapIterator *iterator, int *value) {
  Node *next = NULL;
  if(iterator->node == NULL) {
    iterator->bucket = 0;
    next = iterator->hashmap->buckets[0];
    if(next) {
      iterator->node = next;
      if(value) *value = next->value;
      return next->key;
    }
  }
  else {
    next = iterator->node->next;
    if(next) {
      iterator->node = next;
      if(value) *value = next->value;
      return next->key;
    }
  }

  size_t index = iterator->bucket;
  while(index < iterator->hashmap->buckets_count - 1) {
    next = iterator->hashmap->buckets[++index];
    if(next) {
      iterator->node = next;
      iterator->bucket = index;
      if(value) *value = next->value;
      return next->key;
    }
  }

  return NULL;
}
