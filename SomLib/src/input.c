/*
 * @file         input.c
 * @brief        L3 IA et Apprentissage - 
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-04-09
 */

#include <string.h>
#include "private.h"
#include "hashmap.h"

static const int INIT_LINE_SIZE = 1024;

/**
 * Retourne les données d'un échantillon
 *
 * @param input Les entrées
 * @param sample Le numéro de l'échantillon
 * @return les données de l'échantillon
 */
double *input_data(Input *input, int sample) {
  return input->values + sample * input->nb_dimensions;
}

/**
 * Retourne les données d'un échantillon
 *
 * @param input Les entrées
 * @param sample Le numéro de l'échantillon
 * @return les données de l'échantillon
 */
const double *input_const_data(const Input *input, int sample) {
  return input->values + sample * input->nb_dimensions;
}

/**
 * Détermine le nombre d'éléments (séparés par des virgules) de la ligne.
 *
 * @param line La ligne d'éléments.
 * @return le nombre d'éléments de la ligne.
 */
static int nb_line_elements(char *line) {
  int nb_elements = 0;
  // Prochain jeton
  const char *next = strtok(line, ",");
  // Tant qu'il y a des jetons...
  while (NULL != next) {
    ++nb_elements;
    // Prochain jeton
    next = strtok(NULL, ",");
  }
  return nb_elements;
}

/**
 * Alloue la mémoire nécessaire pour les données d'un fichier d'entrée.
 *
 * @param filename Le nom (chemin) du fichier d'entrée.
 * @return Un pointeur sur une structure d'entrée avec toute la mémoire allouée.
 */
static Input *input_allocate(const char *filename) {
  // Ouverture du fichier texte en lecture
  FILE *file = fopen(filename, "r");
  if (!file) {
    print_last_error("Erreur lors l'ouverture du fichier '%s'", filename);
    return NULL;
  }

  int nb_elements = -1;
  int nb_lines = 0;
  int line_size = INIT_LINE_SIZE;
  char *line = checked_malloc(line_size);
  if(!line) {
    fclose(file);
    return NULL;
  }

  for (;;) {
    if (NULL == fgets(line, line_size - 1, file)) {
      if (feof(file)) break;
      print_last_error("Erreur lors de la lecture du fichier '%s'", filename);
      fclose(file);
      return NULL;
    }

    int length = (int)strlen(line);
    while (length > 1 && !feof(file) && line[length - 1] != '\n') {
      // La ligne est plus grande que la place dans line
      line_size *= 2;
      char *new_line = checked_malloc(line_size);
      strcpy(new_line, line);
      free(line);
      line = new_line;

      if (NULL == fgets(line + length, line_size - length - 1, file)) {
        if (feof(file)) break;
        print_last_error("Erreur lors de la lecture du fichier '%s'", filename);
        fclose(file);
        return NULL;
      }

      length = (int)strlen(line);
    }

    int current_nb_elements = nb_line_elements(line);
    if (nb_elements == -1)
      nb_elements = current_nb_elements;
    else if (nb_elements != current_nb_elements) {
      print_error("La ligne %i du ficher d'entrée '%s' n'a pas le même nombre d'éléments que les autres lignes",
                  nb_lines + 1, filename);
      fclose(file);
      return NULL;
    }

    ++nb_lines;
  }

  fclose(file);

  Input *input = checked_malloc(sizeof(Input));
  if(!input) return NULL;
  input->max_line_size = line_size;
  input->nb_dimensions = nb_elements - 1; // Le nombre d'éléments moins 1 pour la classe
  input->nb_samples = nb_lines;
  input->values = checked_malloc(sizeof(double) * input->nb_dimensions * nb_lines);
  input->classes = NULL;
  input->nb_classes = 0;
  input->samples_classes = checked_malloc(sizeof(int) * nb_lines);

  if(!input->values || !input->samples_classes) {
    free(input);
    return NULL;
  }

  return input;
}

/**
 * Détruit les données d'entrée.
 *
 * @param input Un pointeur sur les  données d'entrée.
 */
void input_destroy(const Input *input) {
  if(!input) return;
  if(input->classes) {
    for(int i = 0; i < input->nb_classes; ++i)
      free((void*)(input->classes[i].name));
  }
  free(input->classes);
  free(input->values);
  free(input->samples_classes);
  free((void*)input);
}

/**
 * Lecture d'un réel.
 *
 * @param token Le mot à convertir.
 * @return Le réel s'il a pu être lu, 0 sinon.
 */
static double read_double(const char *token) {
  char *end = NULL;
  double value = strtod(token, &end);
  if (*end) {
    print_error("Erreur lors de la lecture du nombre '%s'", token);
    return 0;
  }
  return value;
}

static bool set_class_names(Input *input, Hashmap const *hashmap) {
  input->nb_classes = (int)hashmap_count(hashmap);
  input->classes = checked_malloc(sizeof(Class) * input->nb_classes);
  if(!input->classes) return false;

  HashmapIterator *it = hashmap_init_enum(hashmap);
  int index = 0;
  for(const char *name = hashmap_next(it, &index); name; name = hashmap_next(it, &index))
    input->classes[index].name = strdup(name);
  free(it);

  return true;
}

static void set_class_colors(Input *input) {
  Class *cls = input->classes;
  for(int i = 0; i < input->nb_classes; ++i, ++cls) {
    colormap_convert((double)i / (double)input->nb_classes, PALETTE_RAINBOW, &cls->r, &cls->g, &cls->b);
  }
}

/**
 * Lecture d'un tableau de vecteurs depuis un fichier texte suivant un ordre donné.
 * 
 * @param filename Chemin du fichier texte à lire.
*/
Input *input_read(const char *filename) {
  Hashmap *hashmap = hashmap_new();

  Input *input = input_allocate(filename);

  // Ouverture du fichier texte en lecture
  FILE *file = fopen(filename, "r");
  if (!file) {
    perror("Erreur lors l'ouverture du fichier");
    exit(1);
  }

  char line[input->max_line_size + 1];
  double *next_value = input->values;

  // Lecture depuis le fichier
  for (int n_line = 0; n_line < input->nb_samples; ++n_line) {
    if (NULL == fgets(line, sizeof line, file)) {
      if (feof(file)) break;
      perror("Erreur lors de la lecture du fichier d'entrées");
      exit(1);
    }

    int n_element = 0;
    const char *next = strtok(line, ",");
    while (NULL != next) {
      if (++n_element > input->nb_dimensions + 1) {
        fclose(file);
        input_destroy(input);
        print_error("Incohérence lors de la lecture du fichier '', trop d'éléments trouvés", filename, n_element);
        return NULL;
      }

      // On enlève les éventuels espaces, tabulation et retours à la ligne
      char *value = trim_end(strdup(next));
      // Si c'est le dernier élément de la ligne, c'est une classe. Sinon, c'est une valeur.
      if (n_element > input->nb_dimensions) {
        int class_index = 0;
        // Est-ce une classe déjà connue ?
        if(hashmap_contains(hashmap, value))
          class_index = hashmap_get(hashmap, value);
        else {
          class_index = (int)hashmap_count(hashmap);
          hashmap_set(hashmap, strdup(value), class_index);
        }
        input->samples_classes[n_line] = class_index;
      }
      else
        *(next_value++) = read_double(value);

      free(value);
      next = strtok(NULL, ",");
    }
  }

  // On libère le fichier
  fclose(file);

  set_class_names(input, hashmap);
  hashmap_destroy(hashmap);

  set_class_colors(input);

  return input;
}

/**
 * Normalise les données d'entrée.
 *
 * @param input Un pointeur sur les données d'entrée.
 */
void input_normalize(Input *input) {
  double *values = input->values;
  for (int i = 0; i < input->nb_samples; ++i) {
    vector_normalize(values, input->nb_dimensions);
    values += input->nb_dimensions;
  }
}

/**
 * Test si des données sont compatible avec celles d'entrainement.
 *
 * @param trainedInput Un pointeur sur les données d'entrainement.
 * @param input Un pointeur sur les données d'entrée à tester.
 * @return true si les données sont compatibles, false sinon.
 */
bool input_is_compatible(const Input *trainedInput, const Input *input) {
  if(!trainedInput || !input) return false;
  return input->nb_dimensions == trainedInput->nb_dimensions;
}
