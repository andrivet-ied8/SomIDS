/*
 * @file         utils.c
 * @brief        L3 IA et Apprentissage - Utilitaires
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-04-09
 */

#include "private.h"

/**
 * Allocation mémoire. Affiche un message d'erreur en cas d'échec.
 *
 * @param size La taille de la mémoire à allouer.
 * @return Un pointeur sur la mémoire allouée.
 */
void *checked_malloc(size_t size) {
  void *memory = malloc(size);
  if(!memory)
    print_error("Erreur lors d'une allocation mémoire.");
  return memory;
}

/**
 * Enlève les blancs à la fin d'une chaine de caractères.
 *
 * @param text La chaine de caractères.
 * @return La chaine de caractères modifiée.
 */
char *trim_end(char *text) {
  if(!text || !*text) return text;

  size_t length = strlen(text);
  for(char *end = text + length - 1; end >= text; --end) {
    if(*end != ' ' && *end != '\t' && *end != '\r' && *end != '\n')
      break;
    *end = 0;
  }
  return text;
}
