/*
 * @file         hashmap_str.h
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-01-08
 */

#ifndef HASHMAP_STR_H
#define HASHMAP_STR_H

#include <stdbool.h>
#include <stddef.h>

typedef struct Hashmap Hashmap;
typedef struct HashmapIterator HashmapIterator;

Hashmap *hashmap_new(void);
void hashmap_destroy(Hashmap *map);

size_t hashmap_count(const Hashmap *map);
int hashmap_get(const Hashmap *map, const char *key);
bool hashmap_contains(const Hashmap *map, const char *key);

int hashmap_set(Hashmap *map, const char *key, int value);
int hashmap_remove(Hashmap *map, const char *key);

HashmapIterator *hashmap_init_enum(const Hashmap *hashmap);
const char *hashmap_next(HashmapIterator *iterator, int *value);

#endif
