/*
 * @file         vector.c
 * @brief        L3 IA et Apprentissage - 
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-04-09
 */

#include <math.h>
#include "private.h"

/**
 * Initialise un ordre séquentiel.
 * 
 * @param order Le tableau d'entiers représentant l'ordre de parcours.
 * @param nb Nombre d'entiers dans le tableau order.
*/
int *vector_create_sequence(int nb) {
  int *vector = checked_malloc(nb * sizeof(int));
  if(!vector) return NULL;

  // On parcourt le tableau de 0 à nb (exclu)
  for (int i = 0; i < nb; ++i)
    vector[i] = i; // Ordre séquentiel

  return vector;
}

/**
 * Echange deux entiers.
 * 
 * @param first Pointeur sur le premier entier à échanger.
 * @param second Pointeur sur le deuxième entier à échanger.
*/
static void swap_ints(int *first, int *second) {
  int t = *first;
  *first = *second;
  *second = t;
}

/**
 * Mélange un tableau d'entiers.
 * 
 * @param order Le tableau d'entiers à mélanger.
 * @param nb Nombre d'entiers dans le tableau order.
*/
void vector_shuffle(int *vector, int nb) {
  // On parcourt le tableau de 0 à nb (exclu)
  for (int i = 0; i < nb; ++i) {
    // On génère un nombre entre 0 à nb (exclu)
    int j = rand_int() % nb;
    // On échange l'ième nombre avec le jème (aléatoire)
    swap_ints(vector + i, vector + j);
  }
}

/**
 * Normalise un vecteur.
 *
 * @param values Un pointeur sur les valeurs du vecteur.
 * @param nb_values Le nombre de aleurs du vecteur.
 * @return La normal du vecteur.
 */
double vector_normalize(double *values, int nb_values) {
  double sum2 = 0;
  for (int i = 0; i < nb_values; ++i)
    sum2 += pow2d(values[i]);
  // Si la somme est null, toutes les valeurs aussi et cela n'a pas de sens de diviser par la norme
  if(sum2 == 0.) return 0;
  double norm = sqrt(sum2);
  for (int i = 0; i < nb_values; ++i)
    values[i] /= norm;
  return norm;
}

/**
 * Calcul la distance euclidienne entre deux vecteurs.
 *
 * @param values1 Les valeurs du premier vecteur.
 * @param values2 Les valeurs du deuxième vecteur.
 * @param nb_values Le nombre de valeurs des deux vecteurs.
 * @return La distance euclidienne entre les deux vecteurs.
 */
double vector_euclidean_distance(const double *values1, const double *values2, int nb_values) {
  double sum2 = 0;
  for (int i = 0; i < nb_values; ++i)
    sum2 += pow2d(values1[i] - values2[i]);
  return sqrt(sum2);
}

/**
 * Calcul le vecteur moyen d'un ensemble de vecteurs.
 *
 * @param values Un pointeur sur la première valeur du premier vecteur.
 * @param nb_components Le nombre de composantes de chanque vecteurs.
 * @param nb_vectors Le nombre de vecteur.
 * @return Les valeurs du vecteur moyen.
 */
double *vector_compute_means(const double *values, int nb_components, int nb_vectors) {
  // On alloue de la mémoire pour le vecteur de sortie
  double *means = checked_malloc(sizeof(double) * nb_components);
  if(!means) return NULL;

  // On initialise les valeurs à 0
  for (int i = 0; i < nb_components; ++i)
    means[i] = 0;

  const double *value = values;
  // Pour chaque vecteur d'entrée...
  for (int sample = 0; sample < nb_vectors; ++sample) {
    // On ajoute chaque composante aux valeurs du vecteur de sortie
    for (int i = 0; i < nb_components; ++i)
      means[i] += *value++;
  }

  // On divise les valeurs du vecteur de sortie par le nombre de vecteur
  for (int i = 0; i < nb_components; ++i)
    means[i] /= nb_vectors;

  // On retourne le vecteur moyen
  return means;
}

