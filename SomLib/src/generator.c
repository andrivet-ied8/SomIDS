/*
 * @file         generator.c
 * @brief        Génération de nombres aléatoires
 * @details      L3 IA et Apprentissage - Implementation
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-01-01
 */

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <limits.h>
#include <sys/random.h>
#include "generator.h"

/** 
 * Cette version implémente un générateur de nombres pseudo-aléatoires
 * d'après "Squares: A Fast Counter-Based RGN, Bernard Widynski, 2022.
 * https://squaresrng.wixsite.com/rand
 */

static uint32_t squares32(uint64_t ctr, uint64_t key) {
   uint64_t x, y, z;
   y = x = ctr * key; z = y + key;
   x = x*x + y; x = (x>>32) | (x<<32);  // round 1
   x = x*x + z; x = (x>>32) | (x<<32);  // round 2
   x = x*x + y; x = (x>>32) | (x<<32);  // round 3
   return (x*x + z) >> 32;  // round 4
}

static uint64_t squares64(uint64_t ctr, uint64_t key) {
   uint64_t t, x, y, z;
   y = x = ctr * key; z = y + key;
   x = x*x + y; x = (x>>32) | (x<<32);  // round 1
   x = x*x + z; x = (x>>32) | (x<<32);  // round 2
   x = x*x + y; x = (x>>32) | (x<<32);  // round 3
   t = x = x*x + z; x = (x>>32) | (x<<32);  // round 4
   return t ^ ((x*x + y) >> 32);  // round 5
}

static const uint64_t KEY = 0xc8e4fd154ce32f6d; // Généré avec squaresrngv7/keys
static uint64_t counter;
static bool initialized = false;

/**
 * Initialise (si ce n'est pas déjà fait) le compteur avec la source de bruit de la machine.
*/
static void rand_initialize(void) {
    if(initialized) return;

    // On récupère des octets de la source de bruits pour initialiser le compteur
    if(getentropy(&counter, sizeof(counter)) != 0)
        exit(1); // On a pas pu obtenir assez d'octets, on stoppe le programme

    initialized = true;
}

/**
 * Génère aléatoirement un nombre entier entre 0 et INT_MAX.
 * 
 * @note Lors du premier appel, la graine (seed) du générateur de nombres
 * pseudo-aléatoire est initialisée avec la source de bruit de la machine.
 * 
 * @return Un nombre entier entre 0 et RAND_MAX.
*/
int rand_int(void) {
    rand_initialize();
    // On utilise la fonction la plus appropriée suivant la taille du type int
    int value = sizeof(int) <= 4 ? squares32(counter++, KEY) : squares64(counter++, KEY);
    // On retourne un nombre aléatoire toujours positif (bit de poids fort à 0)
    return value & INT_MAX;
}

/**
 * Génère aléatoirement un nombre entier entre 0 et UINT_MAX.
 *
 * @note Lors du premier appel, la graine (seed) du générateur de nombres
 * pseudo-aléatoire est initialisée avec la source de bruit de la machine.
 *
 * @return Un nombre entier entre 0 et RAND_MAX.
*/
unsigned rand_uint(void) {
  rand_initialize();
  // On utilise la fonction la plus appropriée suivant la taille du type int
  return sizeof(int) <= 4 ? squares32(counter++, KEY) : squares64(counter++, KEY);
}

/**
 * Génère aléatoirement un nombre entier dans un interval.
 *
 * @param min Borne inférieur de l'interval.
 * @param max Borne supérieur de l'interval.
 * @return Un nombre entier entre min et max.
 */
int rand_range(int min, int max) {
    return rand_int() / INT_MAX * (max - min) + min;
}

/**
 * Génère aléatoirement un nombre entier dans un interval (0, max).
 *
 * @param max Borne supérieur de l'interval.
 * @return Un nombre entier entre min et max.
 */
unsigned rand_max(unsigned max) {
  return rand_uint() / UINT_MAX * max;
}

/**
 * Génère aléatoirement un nombre flottant dans un interval.
 * 
 * @param min Borne inférieur de l'interval.
 * @param max Borne supérieur de l'interval.
 * @return Un nombre flottant entre min et max.
*/
double rand_double(double min, double max) {
    rand_initialize();
    // On retourne un nombre aléatoire dans l'interval
    return squares64(counter++, KEY) / (double)ULONG_MAX * (max - min) + min;
}
