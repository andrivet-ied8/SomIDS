/*
 * @file         training.c
 * @brief        L3 IA et Apprentissage -
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-04-09
 */

#include "private.h"

/**
 * Décroissance linéaire.
 *
 * @param alpha0 Facteur d`apprentissage de départ.
 * @param iteration Numéro d'itération.
 * @param iterations Nombre total d'itérations.
 * @return Le facteur à appliquer.
 */
static double AlphaLinearDecay(double alpha0, int iteration, int iterations) {
  return alpha0 * (1.0 - (double)iteration / iterations);
}

/**
 * Décroissance inverse.
 *
 * @param alpha0 Facteur d`apprentissage de départ.
 * @param iteration Numéro d'itération.
 * @param iterations Nombre total d'itérations.
 * @return Le facteur à appliquer.
 */
static double AlphaInverseDecay(double alpha0, int iteration, int iterations) {
  UNUSED(alpha0);
  double c = (double)iterations / 50.0;
  return c * iterations / ((double)iteration + c);
}

/**
 * Décroissance exponentielle.
 *
 * @param alpha0 Facteur d`apprentissage de départ.
 * @param iteration Numéro d'itération.
 * @param iterations Nombre total d'itérations.
 * @return Le facteur à appliquer.
 */
static double AlphaExponentialDecay(double alpha0, int iteration, int iterations) {
  return alpha0 * exp(-4.0 * (double)iteration / iterations);
}

/**
 * Décroissance linéaire.
 *
 * @param alpha0 Facteur d`apprentissage de départ.
 * @param iteration Numéro d'itération.
 * @param iterations Nombre total d'itérations.
 * @return Le facteur à appliquer.
 */
static int RadiusLinearDecay(int radius0, int iteration, int iterations) {
  return (int)((double)radius0 + iteration * (1.0 - (double)radius0) / iterations);
}

/**
 * Décroissance inverse.
 *
 * @param alpha0 Facteur d`apprentissage de départ.
 * @param iteration Numéro d'itération.
 * @param iterations Nombre total d'itérations.
 * @return Le facteur à appliquer.
 */
static int RadiusInverseDecay(int radius0, int iteration, int iterations) {
  double c = (double)iterations / (radius0 - 1.0);
  return (int)(c * iterations / ((double)iteration + c));
}

/**
 * Décroissance exponentielle.
 *
 * @param alpha0 Facteur d`apprentissage de départ.
 * @param iteration Numéro d'itération.
 * @param iterations Nombre total d'itérations.
 * @return Le facteur à appliquer.
 */
static int RadiusExponentialDecay(int radius0, int iteration, int iterations) {
  return (int)(radius0 * exp(-3.0 * (double)iteration / iterations));
}

/**
 * Voisinage à bulle.
 *
 * @param distance Distance entre les neurones.
 * @param radius Rayon actuel.
 * @param sigma Ecart type actuel.
 * @return Le facteur à appliquer.
 */
static double NeighborhoodBubble(double distance, int radius, int sigma) {
  UNUSED(sigma);
  return distance <= radius ? 1.0 : 0.0;
}

/**
 * Voisinage gaussien.
 *
 * @param distance Distance entre les neurones.
 * @param radius Rayon actuel.
 * @param sigma Ecart type actuel.
 * @return Le facteur à appliquer.
 */
static double NeighborhoodGaussian(double distance, int radius, int sigma) {
  UNUSED(radius);
  return exp(-pow2d(distance) / (2 * pow2d(sigma)));
}

/**
 * Voisinage en forme de chapeau mexicain.
 *
 * @param distance Distance entre les neurones.
 * @param radius Rayon actuel.
 * @param sigma Ecart type actuel.
 * @return Le facteur à appliquer.
 */
static double NeighborhoodMexicanHat(double distance, int radius, int sigma) {
  UNUSED(radius);
  double factor = 2 / sqrt(3 * sigma) * pow(M_PI, 0.25);
  double sigma2 = pow2d(sigma);
  double distance2 = pow2d(distance);
  return factor * (1 - (distance2 / sigma2)) * exp(-distance2 / (2 * sigma2));
}

/**
 * Voisinage d'Epanechnikov.
 *
 * @param distance Distance entre les neurones.
 * @param radius Rayon actuel.
 * @param sigma Ecart type actuel.
 * @return Le facteur à appliquer.
 */
static double NeighborhoodEpanechnikov(double distance, int radius, int sigma) {
  UNUSED(sigma);
  return distance > radius ? 0 : 3.0 / 4.0 * (1 - (pow2d(distance) / pow2d(radius)));
}


/**
 * Initialise l'entrainement d'une carte de neurones avec des données d'entrée.
 *
 * @param nb_samples Le nombre de données d'entrée.
 * @param nb_neurons Le nombre de neurones.
 * @param nb_iterations Le nombre d'itérations.
 * @param radius0 Le rayon de départ des neurones à activer.
 * @param sigma0 Le l'écart type de départ.
 * @param alpha0 Le facteur d'apprentissage de départ.
 * @param radius_decay_mode Mode d'évolution du rayon.
 * @param sigma_decay_mode Mode d'évolution de sigma.
 * @param alpha_decay_mode Mode d'évolution du facteur d'apprentissage (alpha).
 * @param neighborhood_function Fonction de voisinage.
 * @return Un pointeur sur les données d'entrainement.
 */
Training *training_init(
    int nb_samples, int nb_neurons, int nb_iteration,
    int radius0, int sigma0, double alpha0,
    DECAY_MODE radius_decay_mode,
    DECAY_MODE sigma_decay_mode,
    DECAY_MODE alpha_decay_mode,
    NEIGHBORHOOD_FUNCTION neighborhood_function
) {
  Training *training = checked_malloc(sizeof(Training));
  if(!training) return NULL;

  training->nb_iterations = nb_iteration;
  training->iteration = 0;
  training->radius0 = radius0;
  training->sigma0 = sigma0;
  training->alpha0 = alpha0;
  training->nb_samples = nb_samples;
  training->bmu_list = bmu_create(nb_neurons);
  training->indexes = vector_create_sequence(nb_samples); // séquence d'indices (faux pointeurs)
  if(!training->indexes) {
    free(training);
    return NULL;
  }

  switch(radius_decay_mode) {
    case DECAY_MODE_LINEAR: training->radius_decay = &RadiusLinearDecay; break;
    case DECAY_MODE_INVERSE: training->radius_decay = &RadiusInverseDecay; break;
    case DECAY_MODE_EXPONENTIAL: training->radius_decay = &RadiusExponentialDecay; break;
    default: training->radius_decay = &RadiusLinearDecay; break;
  }

  switch(sigma_decay_mode) {
    case DECAY_MODE_LINEAR: training->sigma_decay = &RadiusLinearDecay; break;
    case DECAY_MODE_INVERSE: training->sigma_decay = &RadiusInverseDecay; break;
    case DECAY_MODE_EXPONENTIAL: training->sigma_decay = &RadiusExponentialDecay; break;
    default: training->sigma_decay = &RadiusLinearDecay; break;
  }

  switch(alpha_decay_mode) {
    case DECAY_MODE_LINEAR: training->alpha_decay = &AlphaLinearDecay; break;
    case DECAY_MODE_INVERSE: training->alpha_decay = &AlphaInverseDecay; break;
    case DECAY_MODE_EXPONENTIAL: training->alpha_decay = &AlphaExponentialDecay; break;
    default: training->alpha_decay = &AlphaLinearDecay; break;
  }

  switch(neighborhood_function) {
    case NEIGHBORHOOD_FUNCTION_BUBBLE: training->neighborhood_function = &NeighborhoodBubble; break;
    case NEIGHBORHOOD_FUNCTION_GAUSSIAN: training->neighborhood_function = &NeighborhoodGaussian; break;
    case NEIGHBORHOOD_FUNCTION_MEXICAN_HAT: training->neighborhood_function = &NeighborhoodMexicanHat; break;
    case NEIGHBORHOOD_FUNCTION_EPANECHNIKOV: training->neighborhood_function = &NeighborhoodEpanechnikov; break;
    default: training->neighborhood_function = &NeighborhoodBubble; break;
  }

  return training;
}

/**
 * Effectue l'étape suivante de l'entrainement.
 *
 * @param map La carte des neurones.
 * @param training Un pointeur sur les données d'entrainement.
 * @param winners Un pointeur des les gagnants de cette étape d'entrainement.
 * @param input Les données d'entrainement.
 * @return true si l'étape suivante de l'entrainement a été effectuée, false s'il n'y a pas de prochaine étape.
 */
bool training_next(Map *map, Training *training, Winners *winners, const Input *input) {
  if(training->iteration >= training->nb_iterations) return false;

  // Calcul du rayon, de sigma et du facteur d'apprentissage
  int radius = training->radius_decay(training->radius0, training->iteration, training->nb_iterations);
  int sigma = training->sigma_decay(training->sigma0, training->iteration, training->nb_iterations);
  double alpha = training->alpha_decay(training->alpha0, training->iteration, training->nb_iterations);

  // On s`assure ue radius et sigma aient un send
  if(radius < 1) radius = 1;
  if(sigma < 1) sigma = 1;

  // On mélange des indices (faux pointeurs)
  vector_shuffle(training->indexes, training->nb_samples);

  // On parcourt toutes les données dans l'ordre des indices
  const int *index = training->indexes;
  for (int i = 0; i < training->nb_samples; ++i, ++index) {
    const double *input_weights = input_const_data(input, *index);
    // On détermine l'entrée la plus proche (BMU)
    int bmu = map_find_bmu(map, input_weights, training->bmu_list);
    winners->neurons[*index] = bmu; // On se souvient du gagnant pour cette donnée d'entrée
    // On met à jour le voisinage
    map_update_neighborhood(map, input_weights, training->neighborhood_function, bmu, radius, sigma, alpha);
  }

  ++training->iteration;
  return true;
}

/**
 * Détruit les données d'entrainement.
 *
 * @param training Les données d'entrainement.
 */
void training_destroy(const Training *training) {
  if(!training) return;

  bmu_destroy(training->bmu_list);
  free(training->indexes);
  free((void*)training);
}

/**
 * Creation d'une liste de BMU ("Best matching unit") d'une taille maximum.
 *
 * @param size Taille maximum de la liste.
 * @return La liste de BMU.
 */
BmuArray *bmu_create(int size) {
  BmuArray *list = checked_malloc(sizeof(BmuArray));
  if(!list) return NULL;
  list->nb_bmus = 0;
  list->indexes = checked_malloc(sizeof(int) * size);
  if(!list->indexes) {
    free(list);
    return NULL;
  }
  return list;
}

/**
 * Destruction d'une liste de BMU.
 *
 * @param list La liste de BMU à détruire.
 */
void bmu_destroy(BmuArray *list) {
  if (!list) return;
  free(list->indexes);
  free(list);
}

