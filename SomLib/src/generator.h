/*
 * @file         generator.h
 * @brief        Génération de nombres aléatoires
 * @details      L3 IA et Apprentissage - Implementation
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-01-01
 */

int rand_int(void);
unsigned rand_uint(void);
int rand_range(int min, int max);
unsigned rand_max(unsigned max);
double rand_double(double min, double max);
