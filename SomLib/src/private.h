/*
 * @file         private.h
 * @brief        L3 IA et Apprentissage - Déclarations
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-04-09
 */

#ifndef PRIVATE_H
#define PRIVATE_H

#include <math.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include "generator.h"
#include "somlib.h"

// Pour éviter les warnings
#define UNUSED(v) (void)v

/** Carte (matrice) de neurones **/

/**
 * Trouve un BMU pour une carte donnée et un vecteur donné.
 * S'il y en a plusieurs, on en choisi un au hasard.
 *
 * @param map La carte de neurones.
 * @param vector Le vecteur à tester.
 * @param bmu_list La liste de BMU à remplir.
 * @return
 */
int map_find_bmu(const Map *map, const double *vector, BmuArray *bmu);

/**
 * Mets à jours les poids des neurones voisins.
 *
 * @param map La carte de neurones.
 * @param input_weights Les valeurs d'entrées.
 * @param neighborhood_function La fonction de calcul du voisinage.
 * @param bmu L'indice de neurone dont on veut mettre à jour le voisinage.
 * @param radius Le rayon du voisinage.
 * @param sigma L'écart type.
 * @param alpha Le facteur d'apprentissage.
 */
void map_update_neighborhood(
    Map *map, const double *input_weights,
    NeighborhoodFunction neighborhood_function,
    int bmu, int radius, int sigma,  double alpha
);

/**
 * Retourne le neurone situé sur une ligne et une colonne donnée.
 *
 * @param map La carte de neurones.
 * @param line La ligne.
 * @param column La colonne.
 * @return Le neurone situé sur la ligne et la colonne donnée.
 */
const Neuron *map_neuron(const Map *map, int line, int column);

/** Liste de "Best matching unit" - Les neurones les plus proches d'une entrée **/

/**
 * Creation d'une liste de BMU ("Best matching unit") d'une taille maximum.
 *
 * @param size Taille maximum de la liste.
 * @return La liste de BMU.
 */
BmuArray *bmu_create(int size);

/**
 * Destruction d'une liste de BMU.
 *
 * @param list La liste de BMU à détruire.
 */
void bmu_destroy(BmuArray *list);

/**
 * Enlève les blancs à la fin d'une chaine de caractères.
 *
 * @param text La chaine de caractères.
 * @return La chaine de caractères modifiée.
 */
char *trim_end(char *text);

static inline double pow2d(double n) { return n * n; }
static inline int pow2i(int n) { return n * n; }

#endif // PRIVATE_H
