/*
 * @file         map.c
 * @brief        L3 IA et Apprentissage - 
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-04-09
 */

#include "private.h"

/**
 * Initialise une valeur suivant le mode d'initialisation.
 *
 * @param init_mode
 * @param index
 * @param means
 * @param min
 * @param max
 * @return
 */
static double init_value(INIT_MODE init_mode, int index, const double *means, double min, double max) {
  switch(init_mode) {
    case INIT_MODE_ZERO: return 0;
    case INIT_MODE_RANDOM: return rand_double(0, 1);
    case INIT_MODE_AVERAGE: return means ? rand_double(means[index] - min, means[index] + max) : 0;
    default: return 0;
  }
}

/**
 * Génère une carte de neurones avec poids aléatoires dans un interval.
 *
 * @param nb_lines Nombre de lignes de la carte.
 * @param nb_columns Nombre de colonnes de la carte.
 * @param nb_weights Nombre de poids pour chaque neurone.
 * @param means Valeurs moyennes pour les poids, ou NULL pour centrer en entre min et max.
 * @param min Valeur minimum (incluse) pour chaque nombre flottant.
 * @param max Valeur maximum (incluse) pour chaque nombre flottant.
*/
Map *map_generate(int nb_lines, int nb_columns, int nb_weights, INIT_MODE init_mode, const double *means, double min, double max) {
  // On alloue de la mémoire pour la carte
  Map *map = checked_malloc(sizeof(Map));
  if(!map) return NULL;
  // On alloue de la mémoire pour les neurones
  map->neurons = checked_malloc(sizeof(Neuron) * nb_lines * nb_columns);
  if(!map->neurons) {
    free(map);
    return NULL;
  }

  // On stocke les informations indispensables pour la suite
  map->nb_lines = nb_lines;
  map->nb_columns = nb_columns;
  map->nb_weights = nb_weights;

  bool failure = false;

  // On parcourt les lignes de la carte
  for (int l = 0; l < nb_lines; ++l) {
    // L'ième ligne de la carte
    Neuron *line = map->neurons + l * nb_columns;
    // On parcourt les colonnes (de neurones) de la carte
    for (int c = 0; c < nb_columns; ++c) {
      // Pointeur sur l'ième neurone de la ligne
      Neuron *neuron = line + c;
      // On alloue de la mémoire pour les poids du neurone
      neuron->weights = checked_malloc(sizeof(double) * nb_weights);
      if(!neuron->weights)
        failure = true;
      else {
        // On parcourt les poids et on génère un nombre aléatoire dans l'interval
        for (int k = 0; k < nb_weights; ++k) {
          // On centre sur la moyenne s'il y en a une, sinon entre min et max
          neuron->weights[k] = init_value(init_mode, k, means, min, max);
        }
      }
      neuron->class_index = 0;
    }
  }

  if(failure) {
    map_destroy(map);
    return NULL;
  }

  return map;
}

/**
 * Retourne le nombre total de neurones dans la carte.
 *
 * @param map La carte de neurones.
 * @return Le nombre total de neurones dans la carte.
 */
int map_nb_neurones(const Map *map) {
  return map->nb_lines * map->nb_columns;
}

/**
 * Retourne la ligne de la carte depuis l'indice d'un neurone.
 *
 * @param map La carte de neurones.
 * @param index L'indice d'un neurone.
 * @return La ligne de la carte.
 */
static int map_row(const Map *map, int index) {
  return index / map->nb_columns;
}

/**
 * Retourne la colonne de la carte depuis l'indice d'un neurone.
 *
 * @param map La carte de neurones.
 * @param index L'indice d'un neurone.
 * @return La colonne de la carte.
 */
static int map_column(const Map *map, int index) {
  return index % map->nb_columns;
}

/**
 * Retourne le neurone situé sur une ligne et une colonne donnée.
 *
 * @param map La carte de neurones.
 * @param line La ligne.
 * @param column La colonne.
 * @return Le neurone situé sur la ligne et la colonne donnée.
 */
const Neuron *map_neuron(const Map *map, int line, int column) {
  return map->neurons + line * map->nb_columns + column;
}

/**
 * Destruction d'une carte.
 *
 * @param map La carte à détruire.
*/
void map_destroy(const Map *map) {
  if(!map) return;

  // On parcourt les neurones et on libère la mémoire pour les poids
  Neuron *neuron = map->neurons; // Premier neurone
  for (int i = 0; i < map_nb_neurones(map); ++i)
    free(neuron++->weights);

  // On libère la mèmoire des neurones
  free(map->neurons);
  // On libère la mémoire de la carte
  free((void*)map);
}

/**
 * Copie une carte de neurones.
 *
 * @param map La carte à copier.
 * @return Une copie de la carte.
 */
Map *map_copy(const Map *map) {
  if(!map) return NULL;

  Map *map_copy = checked_malloc(sizeof(Map));
  if(!map_copy) return NULL;
  // On alloue de la mémoire pour les neurones
  map_copy->neurons = checked_malloc(sizeof(Neuron) * map->nb_lines * map->nb_columns);
  if(!map_copy->neurons) {
    free(map_copy);
    return NULL;
  }

  // On stocke les informations indispensables pour la suite
  map_copy->nb_lines = map->nb_lines;
  map_copy->nb_columns = map->nb_columns;
  map_copy->nb_weights = map->nb_weights;

  bool failure = false;

  Neuron *neuron_copy = map_copy->neurons;
  const Neuron *neuron = map->neurons;
  for (int i = 0; i < map_nb_neurones(map); ++i, ++neuron_copy, ++neuron) {
    neuron_copy->weights = checked_malloc(sizeof(double) * map->nb_weights);
    if(!neuron_copy->weights)
      failure = true;
    else
      memcpy(neuron_copy->weights, neuron->weights, sizeof(double) * map->nb_weights);
    neuron_copy->class_index = neuron->class_index;
  }

  if(failure) {
    map_destroy(map_copy);
    return NULL;
  }

  return map_copy;
}

/**
 * Récupère tous les BMU pour une carte donnée et un vecteur donné.
 *
 * @param map La carte de neurones.
 * @param vector Le vecteur à tester.
 * @param bmu_list La liste de BMU à remplir.
 */
void map_get_all_bmu(const Map *map, const double *vector, BmuArray *bmu_list) {
  bmu_list->nb_bmus = 0;

  const Neuron *neuron = map->neurons;
  double minimum = INFINITY;
  for (int i = 0; i < map_nb_neurones(map); ++i, ++neuron) {
    double d = vector_euclidean_distance(vector, neuron->weights, map->nb_weights);
    // Nouveau minimum ?
    if (d < minimum) {
      // On met la liste de BMU à zéro avec ce minimum
      bmu_list->indexes[0] = i;
      bmu_list->nb_bmus = 1;
      minimum = d;
    } else if (fabs(d - minimum) < 0.001) {
      // On a le même minimum, on l'ajoute à la liste
      bmu_list->indexes[bmu_list->nb_bmus++] = i;
    }
  }
}

/**
 * Trouve un BMU pour une carte donnée et un vecteur donné.
 * S'il y en a plusieurs, on en choisi un au hasard.
 *
 * @param map La carte de neurones.
 * @param vector Le vecteur à tester.
 * @param bmu_list La liste de BMU à remplir.
 * @return
 */
int map_find_bmu(const Map *map, const double *vector, BmuArray *bmu_list) {
  map_get_all_bmu(map, vector, bmu_list);
  int index = (bmu_list->nb_bmus == 1) ? 0 : rand_range(0, bmu_list->nb_bmus - 1);
  return bmu_list->indexes[index];
}

/**
 * Met à jour les poids d'un neurone.
 *
 * @param neuron_weights Les poids du neurone.
 * @param input_weights Les valeurs d'entrées.
 * @param nb_weights Le nombre de poids.
 * @param alpha Le facteur d'apprentissage.
 */
void map_update_weights(double *neuron_weights, const double *input_weights, int nb_weights, double factor) {
  for (int i = 0; i < nb_weights; ++i, ++neuron_weights, ++input_weights)
    *neuron_weights += factor * (*input_weights - *neuron_weights);
}

/**
 * Mets à jours les poids des neurones voisins.
 *
 * @param map La carte de neurones.
 * @param input_weights Les valeurs d'entrées.
 * @param neighborhood_function La fonction de calcul du voisinage.
 * @param bmu L'indice de neurone dont on veut mettre à jour le voisinage.
 * @param radius Le rayon du voisinage.
 * @param sigma L'écart type.
 * @param alpha Le facteur d'apprentissage.
 */
void map_update_neighborhood(
    Map *map, const double *input_weights,
    NeighborhoodFunction neighborhood_function,
    int bmu, int radius, int sigma,  double alpha
) {
  // On calcule les coordonnées du neurone BMU
  int bmu_row = map_row(map, bmu);
  int bmu_column = map_column(map, bmu);
  // Pointeur sur le premier neurone
  Neuron *neuron = map->neurons;

  // On parcourt toutes les lignes de la carte
  for (int r = 0; r < map->nb_lines; ++r) {
    // On parcourt toutes les colonnes de la carte
    for (int c = 0; c < map->nb_columns; ++c, ++neuron) {
      // On calcule la distance entre les deux neurones
      double distance = sqrt(pow(c - bmu_column, 2) + pow(r - bmu_row, 2));
      // On invoque la function de voisinage
      double h = neighborhood_function(distance, radius, sigma);
      // On met à jour les poids du neurone
      map_update_weights(neuron->weights, input_weights, map->nb_weights, alpha * h);
    }
  }
}

/**
 * Classifie les neurones en fonction des valeurs d'entrée.
 * Le neurone aura l'étiquette de la valeur d'entrée la plus proche.
 *
 * @param map La carte de neurones.
 * @param input Les valeurs d'entrée.
 */
void map_classify(Map *map, const Input *input) {
  // Pointeur sur le premier neurone.
  Neuron *neuron = map->neurons;

  // On parcourt tous les neurones
  for (int i = 0; i < map_nb_neurones(map); ++i, ++neuron) {
    double minimum = INFINITY; // Minimum de départ: +inf
    const double *input_values = input->values; // Pointeur sur les valeurs d'entrée
    // On parcourt les valeurs d'entrée
    for (int j = 0; j < input->nb_samples; ++j, input_values += input->nb_dimensions) {
      // On calcule la distance (euclidienne) entre les deux vecteurs
      double d = vector_euclidean_distance(input_values, neuron->weights, map->nb_weights);
      // Nouveau minimum ?
      if (d < minimum) {
        minimum = d; // On met à jour le minimum
        neuron->class_index = input->samples_classes[j]; // On met à jour la classe
      }
    }
  }
}

/**
 * On affiche les poids des neurones de la carte.
 *
 * @param map La carte de neurones à afficher.
 */
void map_display_weights(const Map *map) {
  const Neuron *neuron = map->neurons;
  for (int i = 0; i < map_nb_neurones(map); ++i, ++neuron) {
    printf("(");
    for (int j = 0; j < map->nb_weights; ++j)
      printf("% 0.3f ", neuron->weights[j]);
    printf(") ");
    if (i % map->nb_columns == map->nb_columns - 1)
      printf("\n");
  }
}
