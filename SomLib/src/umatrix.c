/*
 * @file         umatrix.c
 * @brief        L3 IA et Apprentissage
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-04-09
 */

#include "private.h"

/**
 * Retourne les poids pour une ligne et une colonne données.
 *
 * @param umatrix La U-Matrice.
 * @param line La ligne.
 * @param column La colonne.
 * @return Les poids pour la ligne et la colonne données.
 */
static inline double *umatrix_weight(UMatrix *umatrix, int line, int column) {
  return umatrix->weights + line * umatrix->nb_columns + column;
}

/**
 * Alloue la mémoire et génère une U-Matrice à partir d'une carte de neurones.
 *
 * @param map La carte de neurones.
 * @return Un pointeur sur la U-Matrice.
 */
UMatrix *umatrix_generate(const Map *map) {
  UMatrix *umatrix = checked_malloc(sizeof(UMatrix));
  if(!umatrix) return NULL;
  umatrix->nb_lines = 2 * map->nb_lines - 1;
  umatrix->nb_columns = 2 * map->nb_columns - 1;
  umatrix->map_nb_lines = map->nb_lines;
  umatrix->map_nb_columns = map->nb_columns;
  umatrix->weights = checked_malloc(sizeof(double) * umatrix->nb_lines * umatrix->nb_columns);
  if(!umatrix->weights) {
    free(umatrix);
    return NULL;
  }

  // Distance entre les neurones de droite et de gauche de la carte
  for(int l = 0; l < map->nb_lines; ++l) {
    for(int c = 0; c < map->nb_columns - 1; ++c) {
      const Neuron *left = map_neuron(map, l, c);
      const Neuron *right = map_neuron(map, l, c + 1);
      *umatrix_weight(umatrix, l * 2, c * 2 + 1) = vector_euclidean_distance(left->weights, right->weights, map->nb_weights);
    }
  }

  // Distance entre les neurones au dessus et au dessous de la carte
  for(int c = 0; c < map->nb_columns; ++c) {
    for(int l = 0; l < map->nb_lines - 1; ++l) {
      const Neuron *above = map_neuron(map, l, c);
      const Neuron *below = map_neuron(map, l + 1, c);
      *umatrix_weight(umatrix, l * 2 + 1, 2 * c) = vector_euclidean_distance(above->weights, below->weights, map->nb_weights);
    }
  }

  // Moyenne des cellules de droite, de gauche, au dessus et au dessous de la U-Matrix
  for(int l = 0; l < umatrix->nb_lines; ++l) {
    // Les cellules sont décalées d'une ligne à une autre
    for(int c = (l % 2 == 1) ? 1 : 0; c < umatrix->nb_columns; c += 2) {
      double sum = 0;
      int nb = 0;

      // Cellule à gauche
      if(c > 0) {
        sum += *umatrix_weight(umatrix, l, c - 1);
        ++nb;
      }
      // Cellule à droite
      if(c < umatrix->nb_columns - 1) {
        sum += *umatrix_weight(umatrix, l, c + 1);
        ++nb;
      }

      // Cellule au dessus
      if(l > 0) {
        sum += *umatrix_weight(umatrix, l - 1, c);
        ++nb;
      }
      // Cellule en dessous
      if(l <  umatrix->nb_lines - 1) {
        sum += *umatrix_weight(umatrix, l + 1, c);
        ++nb;
      }
      *umatrix_weight(umatrix, l, c) = sum / nb; // Moyenne
    }
  }

  umatrix_normalize(umatrix);
  return umatrix;
}

/**
 * Normalise une U-Matrice.
 *
 * @param umatrix La U-Matrice.
 */
void umatrix_normalize(UMatrix *umatrix) {
  // Première passe pour calculer le minimum et le maximum;
  const double *cweights = umatrix->weights;
  double min = *cweights, max = *cweights;
  for(int i = 1; i < umatrix->nb_lines * umatrix->nb_columns; ++i, ++cweights) {
    if(*cweights < min) min = *cweights;
    if(*cweights > max) max = *cweights;
  }

  // Deuxième passe pour normaliser les poids
  double *weights = umatrix->weights;
  double diff = max - min;
  for(int i = 0; i < umatrix->nb_lines * umatrix->nb_columns; ++i, ++weights) {
    *weights = (*weights - min) / diff;
  }
}

/**
 * Détruit une U-Matrice.
 *
 * @param umatrix La U-Matrice.
 */
void umatrix_destroy(const UMatrix *umatrix) {
  if(!umatrix) return;
  free(umatrix->weights);
  free((void*)umatrix);
}
