/*
 * @file         analyzing.c
 * @brief        L3 IA et Apprentissage -
 * @author       Sebastien Andrivet - 20013599
 * @version      1.0
 * @date         2024-01-08
 */

#include "private.h"

/**
 * Initialisation d'une analyse.
 *
 * @param nb_samples Le nombre de données d'entrée.
 * @param radius0 Le rayon de départ des neurones à activer.
 * @param sigma0 L'écart type des neurones à activer.
 * @param alpha0 Le facteur d'apprentissage de départ.
 */
Analyzing *analyzing_init(int nb_samples, int nb_neurons, int radius0, int sigma0, double alpha0) {
  Analyzing *analyzing = checked_malloc(sizeof(Training));
  if(!analyzing) return NULL;

  analyzing->radius0 = radius0;
  analyzing->sigma0 = sigma0;
  analyzing->alpha0 = alpha0;
  analyzing->nb_samples = nb_samples;
  analyzing->bmu_list = bmu_create(nb_neurons);
  if(!analyzing->bmu_list) {
    free(analyzing);
    return NULL;
  }
  analyzing->indexes = vector_create_sequence(nb_samples); // séquence d'indices (faux pointeurs)
  if(!analyzing->indexes) {
    bmu_destroy(analyzing->bmu_list);
    free(analyzing);
    return NULL;
  }

  return analyzing;
}

/**
 * Analyse.
 *
 * @param map Carte de neurones.
 * @param analyzing Données internes pour l'analyse.
 * @param winners Gagnants (BMU).
 * @param input Données d'entrée.
 */
void analyzing(const Map *map, Analyzing *analyzing, Winners *winners, const Input *input) {
  // On mélange des indices (faux pointeurs)
  vector_shuffle(analyzing->indexes, analyzing->nb_samples);

  // On parcourt toutes les données dans l'ordre des indices
  const int *index = analyzing->indexes;
  for (int i = 0; i < analyzing->nb_samples; ++i, ++index) {
    const double *input_weights = input_const_data(input, *index);
    int bmu = map_find_bmu(map, input_weights, analyzing->bmu_list);
    winners->neurons[*index] = bmu; // On se souvient du gagnant pour cette donnée d'entrée
  }
}

/**
 * Destruction de l'analyse.
 *
 * @param analyzing Données internes pour l'analyse.
 */
void analyzing_destroy(const Analyzing *analyzing) {
  if(!analyzing) return;

  bmu_destroy(analyzing->bmu_list);
  free(analyzing->indexes);
  free((void*)analyzing);
}
