#!/usr/bin/env bash

# Conversion des images PNG en une animation GIF
# Pré-requis: ImageMagick (https://imagemagick.org/index.php)

convert -delay 8 -loop 1 images/*.png images/som.gif
