#!/usr/bin/env gawk -f

BEGIN {
    FS = ","
    OFS = ","
}

{
    switch($42) {
        case "normal": break;
        default: $42 = "attack"; break
    }
}

{
    print $1, $5, $6, $23, $24, $32, $33, $42
}
