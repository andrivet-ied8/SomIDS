#!/usr/bin/env gawk -f

BEGIN {
    FS = ","
    OFS = ","
}

!protocols[$2]++ { $2 = protocols[$2] }
!services[$3]++ { $3 = services[$3] }
!flags[$4]++ { $4 = flags[$4] }

{
    switch($42) {
        case "normal": break;
        default: $42 = "attack"; break
    }
}

{
    print $1, $2, $3, $4, $5, $6, $7, $8, $9,
          $10, $11, $12, $13, $14, $15, $16, $17, $18, $19,
          $20, $21, $22, $23, $24, $25, $26, $27, $28, $29,
          $30, $31, $32, $33, $34, $35, $36, $37, $38, $39,
          $40, $41, $42
}
