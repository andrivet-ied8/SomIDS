#!/usr/bin/env gawk -f

BEGIN {
    FS = ","
}

!seen[$$column]++ {
   array[$column] = $column
}

END {
    asorti(array, sorted);
    for (i = 1; i <= length(sorted); i++) {
        printf("%s\n", sorted[i])
    }
}
